'''
    @file yadda yadda
    '''
    
class Dog:
    '''
    @brief yadda yadda
    '''
    
    def __init__(self, dogName, dogWeight, dogBreed, dogAge=0):
        '''
        @brief yadda yadda
        '''
        # The name of the dog
        self.name = dogName # Assigning input argument (name) 
                            # to class attricute (self.name)
        # The weight of the dog
        self.weight = dogWeight
        # The breed of the dog
        self._breed = dogBreed  # starting a variable name with an underscore
                                # makes it a "private variable" or only
                                # accessible within this same class. Private
                                # variables don't appear in doxygen either.
        # The age of the dog
        self.age = dogAge
        
    def feed (self, food2eat):
        print("That was a tasty " + food2eat.name)
        self.weight += food2eat.calories/3000
        
    def setBreed(self, newBreed):
        self._breed = newBreed
        
        
        
        
        
        
        
        
        
        
