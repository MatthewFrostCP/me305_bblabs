'''@file        Lab0x02.py
   @brief       Create a FSM for controlling the Nucleo LED brightness
   @details     Change the brightness of the Nucleo LED when the user presses
                the D2 button, and have it cycle through three different
                brightness patterns. The patterns include blinking, a sine
                wave, and a sawtooth wave.
                Link to code is here: https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_2/Lab0x02.py
                Video is linked here: https://cpslo-my.sharepoint.com/:f:/g/personal/mtfrost_calpoly_edu/EndILEihL3hNs-37jLaxnyUBrzPiKkonx20vZs6ndKuqxA?e=7ac1dF
   @image       html Lab0x02FSM.jpg "" width=35%
   @author      Matthew Frost
   @date        1/28/2021
'''
import pyb
import micropython
import math
import utime

def onButtonPressFCN(IRQ_src):
    '''
        @brief      Changes the state when button is pressed.
        @details    When the button interrupt argument is called, this is
                    the callback function that is automatically ran. It 
                    changes the state to go from 1 to 3 and back to 1.
        @param      IRQ_src is a predefined variable from the callback function
                    which will be the only possible input to the onButtonPressFCN
                    in order for the button to register it was triggered.
    '''       
    print('Button has been pressed')
    global button_press
    button_press = True
    
    
        
def LED_off():
    '''
    @brief      Turn the LED off.
    @details    This function will set the bandwidth to 0, thus appearing 
                as the LED is off.
    '''    
    t2ch1.pulse_width_percent(0) # Set LED to a bandwidth of 0 (off)

def Blink(t):
    '''
        @brief      Blink the LED at a 2Hz frequency.
        @details    When State 1 is active, the LED will blink at a 2Hz 
                    frequency. This is done my changing the PWM of the LED
                    to stay on for 0.5 second, then off for 0.5 seconds.
        @param      t The time difference from the last button press controls
                    the bandwidth of the LED
    '''    
    t2ch1.pulse_width_percent(100*((t/1000)%1 < 0.5))   
        
def Sine(t):
    '''
        @brief      Fade the LED in a sinusoidal pattern.
        @details    When State 2 is active, the sine wave pattern will run.
                    This adjusts the brightness according the a sine wave with
                    a 50% amplitude and 10 second period.
        @param      t The time difference from the last button press controls
                    the bandwidth of the LED
    '''  
    t2ch1.pulse_width_percent(50*(1 + math.sin(.2*math.pi*(t/1000))))    
    
def Sawtooth(t):
    '''
        @brief      Fade the LED in a sawtooth pattern.
        @details    When State 3 is active, the LED will ramp up in brightness
                    within 1 second, then drop suddenly back to 0% brightness.
                    This is commonly called a sawtooth pattern.
        @param      t The time difference from the last button press controls
                    the bandwidth of the LED
    '''    
    t2ch1.pulse_width_percent(100*((t/1000)%1)) 
    
if __name__ == '__main__':
    # Welcome the user to the Nucleo LED FSM
    print('Thanks for initializing the Nucleo LED Finite State Machine!'
          'To operate, please click the blue button (D2) on the Nucleo. '
          'This will change the LED to show one of three waveforms, either '
          'blinking on and off, a sinusoidal wave, or a sawtooth.')
    
    # Define the specific port on the Nucleo for the LED as pinC13
    ## @brief set pin C13 to a python variable for use in functions
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)      
    # Set the pinA5 on the Nucleo Board
    ## @brief set pinA5 to a python variable for use in functions
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    ## @brief define the Nucleo timer 2 to have a frequency of 20kHz
    tim2 = pyb.Timer(2, freq = 20000)
    ## @brief set the timer to a channel for use in functions
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    # Set up ButtonInt, which is a variable that holds information about 
    # the button interrupt 
    ## @brief the vabirable to holds the button interrupt callback info
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, 
                           callback=onButtonPressFCN)    

    # Set up a byte size for error messages
    micropython.alloc_emergency_exception_buf(100)

    ## @brief state is the current state in the FSM
    #  @details this variable will be changed throughout the program, and 
    #           will be used to set the LED to different waveforms
    state = 0 # always start in the init state
    print('state 0 has been reached')
    # Set the time button was pressed to current time, which will set it to 
    # t = 0 for relative time
    ## @brief records the current time when the button is pressed
    button_time = utime.ticks_ms()
    button_press = False
    
    while True:
        try:
            # If button is pressed, callback function will make
            # button_press = True. If that's the case, change the state as
            # follows
            if button_press == True:
                button_time = utime.ticks_ms() # record the button time
                if state < 3:
                    state += 1 # interate state to next waveform
                    print('Setting State to:', state)
                else:
                    state = 1 # if at the sawtooth wave, go back to square
            
            # Return button_press back to false to wait until button is pressed
            button_press = False        
            
            # record difference in time from when the button was pressed
            t = utime.ticks_diff(utime.ticks_ms(), button_time)
            
            if state==0:
                # Run state 0 (init) code. Set LED to OFF
                LED_off()
                # If user presses button, transition to State 1 (blinking)
        
            elif state==1:
                # Run state 1 (blinking) code
                Blink(t)
                # If user presses button, transition to State 2 (sine wave)
    
            elif state==2:
                # Run state 2 (sine wave) code
                Sine(t)
                # If user presses button, transition to State 3 (sawtooth)
        
            elif state==3:
                # Run state 3 (sawtooth) code
                Sawtooth(t)
                # If user presses button, transition to State 1 (blink)
        
            else:
                # Code to run if state number is invalid,
                # program should ideally never reach here.
                pass

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # turn off LED
    LED_off()
    # Thank the user for coming    
    print('Thanks for testing the program!')