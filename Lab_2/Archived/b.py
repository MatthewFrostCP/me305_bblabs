import pyb

tim2 = pyb.Timer(2, freq=1)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.A5)
t2ch1.pulse_width_percent(50)