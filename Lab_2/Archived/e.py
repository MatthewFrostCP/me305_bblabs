import time
import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim0 = pyb.Timer(2, freq = 2000)
t0ch1 = tim0.channel(1, pyb.Timer.PWM, pin=pinA5)

i = 1
while True:
    if i < 11:
        # pinA5.high()
        t0ch1.pulse_width_percent(100)
        time.sleep(1)
        print('high')
        # pinA5.low()
        t0ch1.pulse_width_percent(0)
        time.sleep(1)
        print('low')
        i += 1
    