import pyb
import numpy as np
import math

pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim0 = pyb.Timer(2, freq = 20000)
t0ch1 = tim0.channel(1, pyb.Timer.PWM, pin=pinA5)

t = np.linspace(0,10,100)
m = 0
y = []

while m <= len(t)-1:
    ym = 50*(1 + math.sin(.2*math.pi*float(t[m])))
    y.append(float(ym))
    m += 1         

i = 0
while True:
    if i <= len(t)-1:
        t0ch1.pulse_width_percent(float(y[i]))
        time.sleep(0.1)
        i += 1
    else: 
        i = 0