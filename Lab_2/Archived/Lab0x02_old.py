'''@file        Lab0x02.py
   @brief       Create a FSM for controlling the Nucleo LED brightness
   @details     Change the brightness of the Nucleo LED when the user presses
                the D2 button, and have it cycle through three different
                brightness patterns. The patterns include blinking, a sine
                wave, and a sawtooth wave.
                Link to code is here: 
   @author      Matthew Frost
   @date        1/28/2021
   @copyright   License Info Here
'''

import time
import pyb
import micropython
import math
micropython.alloc_emergency_exception_buf(100)

def onButtonPressFCN(IRQ_src):
    '''
        @brief      Changes the state when button is pressed.
        @details    When the button interrupt argument is called, this is
                    the callback function that is automatically ran. It 
                    changes the state to go from 1 to 3 and back to 1.
    '''       
    print('Button has been pressed')
    global state
    if state < 3:
        state += 1
    else:
        state = 1
        
def LED_off(cmd):
    '''
    @brief      Turn the LED off.
    @details    This function will set the bandwidth to 0, thus appearing 
                as the LED is off.
    '''    
    if cmd=='Off':
        # Set timer to prepare for incoming commands.
        tim0 = pyb.Timer(2, freq = 2000)
        t0ch1 = tim0.channel(1, pyb.Timer.PWM, pin=pinA5)
        t0ch1.pulse_width_percent(0) # Set LED to a bandwidth of 0 (off)

def Blink(cmd):
    '''
        @brief      Blink the LED at a 2Hz frequency.
        @details    When State 1 is active, the LED will blink at a 2Hz 
                    frequency.
    '''    
    if cmd=='Blink':
        # Set timer to prepare for incoming commands.
        # tim1 = pyb.Timer(2, freq = 2000)
        # t1ch1 = tim1.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        # # while in this function, blink LED for half a second, then turn it
        # # off for half a second.
        # t1ch1.pulse_width_percent(100) # turn on
        # time.sleep(.5)
        # t1ch1.pulse_width_percent(0) # turn off
        # time.sleep(.5)
        # tim1 = pyb.Timer(2, freq=1)
        # t1ch1 = tim1.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.A5)
        # t1ch1.pulse_width_percent(50)
        tim1 = pyb.Timer(2, freq = 2000)
        t1ch1 = tim1.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        global i 
        
        if i <= 10:
            if i <= 5:
                t1ch1.pulse_width_percent(100) # turn on
            else: 
                t1ch1.pulse_width_percent(0) # turn off
        else: 
            i = 0
            t1ch1.pulse_width_percent(100) # turn on
    time.sleep(0.1)
    i += 1
    return i
        
def Sine(cmd, state):
    '''
        @brief      Fade the LED in a sinusoidal pattern.
        @details    When State 2 is active, the sine wave pattern will run.
                    This adjusts the brightness according the a sine wave with
                    a 50% amplitude and 10 second period.
    '''    
    if cmd=='Sine':
        # Set  timer to prepare for incoming commands.
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        global t
        
        # While in state two, display a brightness that moves in a sinusoidal
        # pattern, then increment time. 
        t2ch1.pulse_width_percent(50*(1 + math.sin(.2*math.pi*t)))
        time.sleep(.1)
        t += 0.1
        return t
        
    
def Sawtooth(cmd):
    '''
        @brief      Fade the LED in a sawtooth pattern.
        @details    When State 3 is active, the LED will ramp up in brightness
                    within 1 second, then drop suddenly back to 0% brightness.
                    This is commonly called a sawtooth pattern.
    '''    
    if cmd=='Sawtooth':
        # Set timer to prepare for incoming commands.
        tim3 = pyb.Timer(2, freq = 20000)
        t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        global ts
        
        # While in State 3, display a brightness that is consistent with a 
        # sawtooth pattern, then increment time and redisplay.
        if ts <= 1:
            t3ch1.pulse_width_percent(ts*100)
            time.sleep(.1)
            ts += 0.1
        else: 
            ts = 0
            return ts
    

if __name__ == '__main__':
    # Welcome the user to the Nucleo LED FSM
    print('Thanks for initializing the Nucleo LED Finite State Machine!'
          'To operate, please click the blue button (D2) on the Nucleo. '
          'This will change the LED to show one of three waveforms, either '
          'blinking on and off, a sinusoidal wave, or a sawtooth.')
    
    # Define the specific port on the Nucleo for the LED as pinC13
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)      
    print('pinC13 has been set')
    
    # Set the pinA5 on the Nucleo Board
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

    state = 0 # always start in the init state
    print('state 0 has been reached')
    
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, 
                           callback=onButtonPressFCN)
    
    while True:
        try:
            if state==0:
                # Run state 0 (init) code. Set LED to OFF
                while state==0:
                    LED_off('Off')
                print('Setting LED to State 1')
                # If user presses button, transition to State 1 (blinking)
        
            elif state==1:
                # Run state 1 (blinking) code
                i = 0
                while state==1:
                    Blink('Blink')
                print('Setting LED to State 2')
                # If user presses button, transition to State 2 (sine wave)
    
            elif state==2:
                t = 0
                # Run state 2 (sine wave) code
                while state==2:
                    Sine('Sine', state)
                print('Setting LED to State 3')
                # If user presses button, transition to State 3 (sawtooth)
        
            elif state==3:
                ts = 0
                # Run state 3 (sawtooth) code
                while state==3:
                    Sawtooth('Sawtooth')
                print('Setting LED to State 1')
                # If user presses button, transition to State 1 (blink)
        
            else:
                # Code to run if state number is invalid,
                # program should ideally never reach here.
                pass

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Thank the user for coming    
    print('Thanks for testing the program!')