myVar = 0
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)  

def myCallback(IRQ_source):
    global myVar
    myVar += 1
    print(myVar)


if __name__ == '__main__':
    
    while True: 
        try:
            while True: 
                ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, 
                       callback=myCallback)
                
                print('you have exited the callback function')
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break




