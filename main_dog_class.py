import dog
import food

myDog = dog.Dog('Chloe', 65, 'Australian Cattle Dog', 8)
yourDog = dog.Dog('Killer', 6, 'Maltese', 9)

anotherDog = dog.Dog('Fido', 12, 'Terrier')

yetAnotherDog = dog.Dog('Spot', 10, dogAge=9)

someFood = food.food("Kibbles 'n 'bits", 500)

print(myDog.name + ' weighs ' + str(myDog.weight))
myDog.feed(someFood)
print(myDog.name + ' weighs ' + str(myDog.weight))