import time

# Function definitions go here






# Main program / test program begin:
# This code only runs if the script is executed as main by pressing play
#but doesn not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    print('Before')
    state = 0 # initial state is the init state
    
    while True: 
        try: 
            # main program code goes here
           if state==0:
               # run state 0 (init) code
               pass
           
           elif state==1:
               # run state 1 (moving left) code
               pass
           
           elif state==2:
               # run state 2 (stopped at left) code
               pass
           
           elif state==3:
               # run state 3 (moving right) code
               pass
           
           elif state==4:
               # run state 4 (stopped at right) code
               pass
           else:
               # code to run if state number is invilid
           
               
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization goes here
    print('After')