'''@file        Elevator_Example.py
   @brief       Simulate an elevator between two floors
   @details     Implements a finite state machine, shown below, to simulate the
                behavior of the elevator. The user can go between two floors.
                Find code here: https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Homework/Elevator_Example.py
   @author      Matthew Frost
   @date        1/27/2021
   @copyright   License Info Here
'''

import time
import random

def motor_cmd(cmd):
    '''
    @brief      Commands the motor to move up, down, or stop
    '''
    if cmd==0:
        print('Motor is stopped')
    elif cmd==1:
        print('Elevator moving up to Floor 2')
    elif cmd==2:
        print('Elevator moving down to Floor 1')
    
def Button_1():
    '''
    @brief      Signifies if Button_1 is on or off
    @return     A random true or false as to if the button has been pressed
    '''
    return random.choice([True, False]) # Randomly returns True or False

def Button_2():
    '''
    @brief      Signifies if Button_2 is on or off
    @return     A random true or false as to if the button has been pressed
    '''    
    return random.choice([True, False]) # Randomly returns True or False

def First_Floor():
    '''
    @brief      Signifies if elevator has reached Floor 1
    @return     A random true or false as to if the Floor 1 has been reached
    '''    
    return random.choice([True, False]) # Randomly returns True or False

def Second_Floor():
    '''
    @brief      Signifies if elevator has reached Floor 2
    @return     A random true or false as to if the Floor 2 has been reached
    ''' 
    return random.choice([True, False]) # Randomly returns True or False





if __name__ == "__main__":    
    # Welcome the user to the elevator simulation
    print('Thanks for entering the elevator!')
    
    state = 0 # initial state is the init state
    
    # Begin finite state machine
    while True: 
        try: 
            if state==0:
                # Run state 0 (init) code.
                motor_cmd(2) # Start by moving the elevator down.
                if First_Floor():
                    print('Floor 1 has been reached')
                    state=1 # Updating state for next iteration.                
        
            elif state==1:
                # Run state 1 (stopped at Floor 1) code until button is pressed.
                motor_cmd(0)
                if Button_2():
                    print('You chose to go to Floor 2')
                    state = 2 # Transition to next state.
        
            elif state==2:
                # Run state 2 (moving up).
                motor_cmd(1)
                if Second_Floor():
                    print('Floor 2 has been reached')
                    state=3 # Updating state for next iteration.
        
            elif state==3:
                # Run state 3 (stopped at Floor 2) code until button is pressed.
                motor_cmd(0)
                if Button_1():
                    print('You chose to go to Floor 1')
                    state = 0 #Transition to next state.
        
            else:
                # Code to run if state number is invalid,
                # program should ideally never reach here.
                pass
        
            # Slow down execution of FSM so we can see output in console  . 
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired.
            break
    
    # Thank the user for riding the elevator.
    print('Thanks for riding the elevator!')