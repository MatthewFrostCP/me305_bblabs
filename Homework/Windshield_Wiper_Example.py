'''@file        Windshield_Wiper_Example.py
   @brief       Simulates a set of windshield wipers
   @details     Implements a finite state machine, shown below, to simulate the
                behavior of a set of wipers.
   @author      Matthew Frost
   @date        1/19/2021
   @copyright   License Info Here
'''

import time
import random


def motor_cmd(cmd):
    '''
        @brief Commands the motor to move or stop
        @param The command to give the motor

    '''
    if cmd=='FWD':
        print('Mot FWD')
    elif cmd=='REV':
        print('Mot REV')
    elif cmd=='STOP':
        print('Mot STOP')
    
def L_sensor():
    return random.choice([True, False]) # Randomly returns True or False

def R_sensor():
    return random.choice([True, False]) # Randomly returns True or False

def go_button():
    return random.choice([True, False]) # Randomly returns True or False


# Main program / test program begin:
# This code only runs if the script is executed as main by pressing play
#but doesn not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    print('Before')
    state = 0 # initial state is the init state
    
    while True: 
        try: 
            # main program code goes here
            if state==0:
               # run state 0 (init) code
                motor_cmd('FWD')
                state = 1 # Updating state for next iteration
                print('S0')
           
            elif state==1:
                # run state 1 (moving left) code
                print('S1')
                # If we are at the left, stop the motor and transition to S2
                if L_sensor():
                   motor_cmd('STOP')
                   state=2
           
            elif state==2:
                # run state 2 (stopped at left) code
                print('S2')
                if go_button():
                    motor_cmd('REV')
                    state=3
           
            elif state==3:
                # run state 3 (moving right) code
                print('S3')
                if R_sensor():
                    motor_cmd('STOP')
                    state=4
           
            elif state==4:
                # run state 4 (stopped at right) code
                print('S4')
                motor_cmd('FWD')
                state = 1 # Updating state for next iteration
           
            else:
                # code to run if state number is invalid
                # Program should ideally never reach here
                pass
           
            # Slow down execution of FSM so we can see output in console   
                time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization goes here
    print('After')