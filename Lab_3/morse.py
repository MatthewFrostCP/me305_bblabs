# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 10:13:16 2021

@author: matth
"""
def to_morse(s):
    return ' '.join(CODE.get(i.upper()) for i in s)

if __name__ == "__main__":
    CODE = {'A': '. -',     'B': '- . . .',   'C': '- . - .', 
        'D': '- . .',    'E': '.',      'F': '. . - .',
        'G': '- - .',    'H': '. . . .',   'I': '. .',
        'J': '. - - -',   'K': '- . -',    'L': '. - . .',
        'M': '- -',     'N': '- .',     'O': '- - -',
        'P': '. - - .',   'Q': '- - . -',   'R': '. - .',
        'S': '. . .',    'T': '-',      'U': '. . -',
        'V': '. . . -',   'W': '. - -',    'X': '- . . -',
        'Y': '- . - -',   'Z': '- - . .'
        }
    
    output = to_morse('hello')
    print(output)
