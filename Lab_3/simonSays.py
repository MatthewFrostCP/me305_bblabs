import random
import pyb
import micropython
import utime

class simonSays:
    
    def __init__(self, DBG_flag=True):
        # current state of the finite state machine
        self.state = 0
        self.DBG_flag = DBG_flag
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.buttonPress = False
        self.pin_value = False
        self.buttonInt = pyb.ExtInt(self.pinC13, 
                                    mode=pyb.ExtInt.IRQ_RISING_FALLING,
                                    pull=pyb.Pin.PULL_NONE, 
                                    callback=self.onButtonPressFCN)
        # time stamp for last iteration of the task in microseconds
        
        
    def run(self):
        if self.state == 0:    
            self.LED_off()
            if self.buttonPress == True:
                self.pin_value = Pin.value(True)
                self.state = 1
        elif self.state == 1:
            pass
        elif self.state == 2:
            pass
        elif self.state == 3:
            pass
        else:
            # code to run if state number is invalid
            # Program should ideally never reach here
            pass    
        
    def onButtonPressFCN(self, IRQ_src):      
        print('Button has been pressed')
        self.buttonPress = not self.buttonPress
    
    def LED_blink(self, pin_value):
        pass
        
    def LED_off(self):
        self.pinA5.low()
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    