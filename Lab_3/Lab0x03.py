'''@file        Lab0x03.py
   @brief       Create a FSM for controlling the
   @details     Change the 
                Link to code is here: 
                Video is linked here: 
   @image       html Lab0x03FSM.jpg "" width=35%
   @author      Matthew Frost
   @date        1/28/2021
'''

from simonSays import simonSays

# Main program / test program begin:
# This code only runs if the script is executed as main by pressing play
#but doesn not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    try1 = simonSays(DBG_flag=True)
    
    while True: 
        try: 
            # print('T1:')
            try1.run()
            # print('T2:')
            # task2.run()
            # Slow down execution of FSM so we can see output in console   
            # time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization goes here
    print('After')