'''
@file       game.py
@brief      Class for simon says game
@details    detailed description here
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on 02/04/21 \n Last modified on 02/04/21
'''


import utime
import random
import micropython
import pyb


class game:
    
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)                      # create pin object for PC13, blue button
    ## @brief      Pin variable for LED 
    #  @details    Similar to `pinC13`, but for the output LED. 
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)                        # create pin object for PA5, LED
    ## @brief       Pin variable for pyb timer     
    #  @details     To use pulse width modulation of the LED, we need a timer.
                           # define pin timer
    ## @brief       Channel variable for pyb timer
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)       # create timer channel
    
    ## @brief       Callback interrupt variable
    #  @details     When the blue button is pressed or released, it triggers the callback variable,
    #               running the callback function `onButtonPressFCN`.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    
    
    S0_INIT = 0                   # basic init state
    S1_BLINK_LED = 1              # blink n = 1 to n = i letter
    S2_BUTTON_PRESSED = 2         # counting time that button has been pressed
    S3_PROC_BUTTON_PRESSED = 3    # post-process state 2
    S4_BUTTON_RELEASED = 4        # counting time that button has been released
    S5_PROC_BUTTON_RELEASED = 5   # post-process state 4
    S6_INCORRECT_INPUT = 6        # user inputted incorrectly, show score/ask to play again
    S7_END_GAME = 7               # end game
    S8_USER_WON = 8 
    # constructor definition
    
    def __init__(self, IRQ_src, setNum, period, maxTime = 5, debugFlag = False):      # setNum is how many sets per round
        
        # define pin stuff
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13)  
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5) 
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)      
    

        ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING|pyb.ExtInt.IRQ_FALLING , pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
        
        
        
                               
        self.state = 0
        self.runs = 0
        self.round = 1
        self.set = 1
        

        self.buttonPress = False        # button initially not pressed
        
        self.setNum = setNum
        
        # timing setup
        self.lastTime = utime.ticks_us()        # time stamp of last iteration in μs
            
        self.period = period        # run based on defined frequency
            
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        self.debugFlag = debugFlag
        
        self.buttonTime = utime.ticks_ms()
        
        self.currentTime = utime.ticks_ms()
        
        self.maxTime = maxTime  # the maximum amount of time before the program
                                # boots you out for taking too long (in seconds)
        
        self.nucleoScore = 0 # starting score for the nucleo
        self.userScore   = 0 # starting score for the user
        
        
        
    # main run method  
    def run(self):
        # timing setup
        
        thisTime = utime.ticks_us()
        self.currentTime = utime.ticks_ms()
        
        
        # if button is pressed, record the time that it was pressed
        if self.buttonPress == True:
            self.buttonTime = utime.ticks_ms()
        
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:       # if time exceeds nextTime, the time stamp for when a new
                                                                # iteration should begin, then need to calculate new nextTime
            # need to figure out how timing system is going to work
            self.nextTime = utime.ticks_add(self.lastTime, self.period)       # update what next time should be                                             
                
            if self.debugFlag == True:        # if debugging is on, want to print more stuff
                  print(': STATE' + str(self.state) + ': ROUND' + str(self.round) + ': SET' + str(self.set))
        
            if self.state == self.S0_INIT:
                pass
                # reset anything that needs to be reset, this state is only entered when the
                # program is first run
            
            elif self.state == self.S1_BLINK_LED:
                # print which set the LED is on (1-# of sets specified)
                print('LED is blinking set #: ')
                
                # start a countdown timer (not using code blocking, just based on time)
                
                # blink LED code goes here:
                
                # if button is pressed, transition to the user input code (S2)
                if self.buttonPress == True:
                    self.state = self.S2_BUTTON_PRESSED
                
            
            elif self.state == self.S2_BUTTON_PRESSED:
                self.elapsedDownTime = utime.ticks_diff(self.currentTime, self.buttonTime)
                
                if self.debugFlag == True:
                    print('S2, button is held down')
                    print(self.elapsedDownTime) # see amount of time pressed
                
                if self.buttonPress == False:
                    self.state = self.S3_PROC_BUTTON_PRESSED
                    
                if self.elapsedDownTime >= self.maxTime:
                    self.state = self.S6_INCORRECT_INPUT
                # print statement that button has been pressed
                # count the time that the button has been pressed
            
            elif self.state == self.S3_PROC_BUTTON_PRESSED:
                
                # Convert time elapsed into a dot or a dash representation
                if self.unit + self.tol <= self.elapsedDownTime <= self.unit + self.tol:
                    self.value = '.'
                elif 3*self.unit + self.tol <= self.elapsedDownTime <= 3*self.unit + self.tol:
                    self.value = '-'
                else:
                    self.state = self.S6_INCORRECT_INPUT
                
                # compare the dot/dash representation to the expected value
                if self.value == # the jth element of the pattern & self.idx < length(self.pattern)
                    self.state = S4_BUTTON_RELEASED
                else:
                    self.state = S8_USER_WON
            
            elif self.state == self.S4_BUTTON_RELEASED:
                self.elapsedUpTime = utime.ticks_diff(self.currentTime, self.buttonTime)
                
                if self.debugFlag == True:
                    print('S4, button is released')
                    print(self.elapsedUpTime) # see amount of time pressed
                
                if self.buttonPress == True:
                    self.state = self.S5_PROC_BUTTON_RELEASED
                    
                if self.elapsedDownTime >= self.maxTime:
                    self.state = self.S6_INCORRECT_INPUT
                # Print that the button has been released
                # count the time that the button has been released
            
            elif self.state == self.S5_PROC_BUTTON_RELEASED:
                 # Convert time elapsed into a dot or a dash representation
                if self.unit + self.tol <= self.elapsedUpTime <= self.unit + self.tol:
                    self.value = '_'
                elif 3*self.unit + self.tol <= self.elapsedUpTime <= 3*self.unit + self.tol:
                    self.value = '/'
                else:
                    self.state = self.S6_INCORRECT_INPUT
                
                # I DON'T THINK WE NEED THIS CODE BUT JUST IN CASE:
                # # compare the dot/dash representation to the expected value
                # if self.value == # the jth element of the pattern & self.idx < length(self.pattern)
                #     self.state = S2_BUTTON_PRESSED
                # else:
                #     self.state = S8_USER_WON
            elif self.state == self.S6_INCORRECT_INPUT:
                self.nucleoScore += 1
                if self.debugFlag == True:
                    print('S6, User has messed up')
                    print('Nucleo: ' + str(self.nucleoScore) ', User: ' + str(self.userScore))
                # if user doesn't match pattern correctly, display error message
                # display the score from the rounds
                # prompt user for input on whether they would like to continue playing
            
            elif self.state == self.S7_ENDGAME:
                pass
                # display "thanks for playing" 
            

            
            
            
            
            
            
    # state transition method
    def transitionTo(self, newState):
        if self.debugFlag == True:
            print( str(self.state) + '->' + str(newState) )
        self.state = newState               # now that transition has been declared, update state
        
        
    # button interupt method
    def onButtonPressFCN(self, IRQ_src):
        
    # want to enter this method whenever button is pressed or released
    # button is initially set to false in constructor
        
        # if button is pressed, switch to true, but if released from True, then go back to false
        self.buttonPress = not self.buttonPress
        
    
    # LED pattern generation method
    def patternGenerate(self):
        pass
        # want this method to be able to randomly generate patterns for the LED
        
    
    # LED operation method
    def LED(self):
        pass
        # should be able to set the LED on or off
        
        
    # comparision method, comparing LED output to button user input
    def compare(self):
        pass
        # should give simple output of True or False, 
        # depending on whether input matches output correctly (within certain tolerance range)  
    
        
 