## @file mainpage.py
#
## @mainpage ME 305 Lab: Final Project
#
#  For our final lab in ME 305, we were tasked with several new mechatronics
#  components and techniques to finish out the quarter. These included using
#  serial communication between the Spyder Console and Nucleo board. We also
#  got some experience with DC motors, quadrature encoders, and PI controllers
#  to do positional and speed tracking. The lab was broken up into four weeks, 
#  with each week adding a new layer of complexity to the overall term project.
#  Below is a list of the different weeks, with a summary of the specific task
#  and any deliverables or results.
#  
#  -- @subpage Week1
#  
#  -- @subpage Week2
#  
#  -- @subpage Week3
#  
#  -- @subpage Week4
#  
#  In total, this project provided a challenging, yet rewarding, exposure to
#  common hardware within the mechatronics world. In the future, this code and
#  design process will be uesful for tackling programming projects, as learning 
#  the necessary state diagram drawing techniques, program scaffolding, and 
#  task diagrams to interfacae between scripts will be universal in future
#  programming projects.
#  
#  The final code can be seen on my online repository. Use the following link
#  in order to view the code:
#  
#  https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final
#  
#  
#  
#  
#  
##############################################################################  
## @page Week1 Week 1: Familiarization with Serial Communication
#  
#  In the first week of the lab final, we were tasked with setting up the 
#  serial communication between the Spyder Console and the Nucleo. Instead
#  of relying on time to send data at regular intervals, it's more important
#  to be able to request data whenever wanted and get the data in a timely
#  manner. To implement this, the front end (Spyder Console) was set up to
#  interact through the serial port. The data was sent through one of the USBs,
#  and read on the board. In order to send data back, a script was created to
#  send data through the UART port, which would send it back through serial 
#  to the front end. In addition, the two USBs allowed for debugging commands
#  to show up in PuTTy, whereas only specific commands would be displayed in
#  the Spyder Console. This is important because a user doesn't want to see
#  all the commands and debugging code when testing the final product. 
#  
#  To test the program, when the letter g was pressed, a command was sent to 
#  the Nucleo to start sampling data. For this first week, this just took in 
#  time and found a value from an exponentially decaying periodic function. 
#  Once the time limit either ran out, or if the s key was pressed, the data
#  collection stopped. The uiTask.py, which was the file that had the 
#  exponetially decaying function on it, then sent the data through serial 
#  back to the front end for plotting. 
#  
#  Code for this part of the project has since been written over, but their 
#  final forms can be viewed at the following files. 
#  
#  --PC_FrontEnd.py 
# 
#  --uiTask.py
#  
#  Additionally, a plot of the response to the function was created to ensure
#  that the data collection method and keyboard commands were working. This 
#  can be seen in Figure 1, the function correctly was sampled at a reasonable
#  interval (as can be seen by the resolution of the function).
#  
#  
#  @image      html dataPlot.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 1.** Week 1 Deliverable: Exponential Decay of Periodic Function </CENTER> \n
#  
#  
#  Below is a summary of the state diagrams utilized for the week. These stayed
#  fairly consistent between weeks (the overall flow stayed the same, we were
#  just exporting/sending more data with each week).
#  
#  @image      html Front_End_FSM_Wk1.jpg " " width = 10%
#  @details    \n <CENTER> **Figure 2.** Week 1 Deliverable: Front End State Diagram </CENTER> \n
#  
#  @image      html UI_Task_FSM_Wk1.jpg " " width = 20%
#  @details    \n <CENTER> **Figure 3.** Week 1 Deliverable: UI Task State Diagram </CENTER> \n
#  
##############################################################################
## @page Week2 Week 2: Introduction to Quadrature Encoders
#  
#  In the second week of the final project, we got our first taste for using
#  the encoders in our hardware kit. Using the method derived in class, we
#  wrote an encoder class (seen in encoder.encoder.py) that implemented an 
#  algorithm to prevent overflow and underflow as the motor shaft was spun.
#  For the first week, the motors weren't powered, and the shafts were 
#  handspun to test the implementation. More commands were added to our front
#  end, such as a 'p' to get the position, 'd' to get the delta in ticks, 'v'
#  to get the velocity, and 'z' to zero the encoder. By spinning the encoder
#  with one hand and pressing key commands with the other, the system could be
#  tested to ensure that the keyboard commands correctly sent information to 
#  the Spyder Console window for printing.
#  
#  Code for this part of the project has since been written over, but their 
#  final forms can be viewed at the following files.
#  
#  --PC_FrontEnd.py 
#  
#  --uiTask.py
#  
#  --encoder.py
#  
#  Below is a summary of the state diagrams utilized for the week. These stayed
#  fairly consistent between weeks (the overall flow stayed the same, we were
#  just exporting/sending more data with each week).
#  
#  @image      html Front_End_FSM_Wk2.jpg " " width = 10%
#  @details    \n <CENTER> **Figure 1.** Week 2 Deliverable: Front End State Diagram </CENTER> \n
#  
#  @image      html UI_Task_FSM_Wk2.jpg " " width = 20%
#  @details    \n <CENTER> **Figure 2.** Week 2 Deliverable: UI Task State Diagram </CENTER> \n
#  
#  @image      html Ctrl_Task_FSM_Wk2.jpg " " width = 30%
#  @details    \n <CENTER> **Figure 3.** Week 2 Deliverable: Controller Task State Diagram </CENTER> \n
# 
#  
##############################################################################
## @page Week3 Week 3: Introduction to P Controller for Spinning Motors
#  
#  In the third week of the final project, the motors actually got to spin.
#  Since this was the most basic form of spinning the motors, A P controller
#  was implemented to match a step input to the motor (essentially, set the
#  motor to a specific speed) using a P controller. This required starting a 
#  few more classes and program structure than the previous weeks. First, the
#  encoder task (created in Week 2) turned into a controller task (as seen 
#  here: ctrlTask.py). This interfaced with a motor, encoder, and controller
#  driver, which, combined, created a working motor controller system to 
#  react to the step input. All of the files (albiet it, in their final form)
#  are listed here. 
#  
#  --PC_FrontEnd.py 
#  
#  --main.py
#  
#  --uiTask.py
#  
#  --ctrlTask.py
#  
#  --encoder.encoder
#  
#  --controllerdriver.controllerdriver
#  
#  --motor.motorDriver
#  
#  --shares.py
#  
#  The workflow was that the main.py file created instances of the uiTask.py and
#  ctrlTask.py classes. Within the ctrlTask.py file, instances of the encoders
#  motors, and controller drivers were created. Since more commands were now
#  being shared, shares.py was implemented in order to have a space for values
#  and commands that would trigger actions in the controller task.
#  
#  There were several state and task diagrams created, which can be viewed
#  within the specific files themselves. The files containing state diagrams 
#  include the ctrlTask.py, uiTask.py, and PC_FrontEnd.py. There also was a
#  task diagram within shares.py that helps show the utility of the script.
#  Another major portion of the lab was dedicated to tuning the proportionality
#  constant, Kp, in order to try and get the system to match the speed faster
#  and with less oscillation. Below are a few plots that show the response of 
#  the system, and the figure captions highlight the different Kp values that 
#  were tested. The last figure shows the best option found, and although it
#  still has some oscillation, this was due to a few factors that Week4 will 
#  hopefully fix. First, without a Ki (or Kd if we were doing a PID controller),
#  it's hard to get a perfect system, because if the system overshoots, it will
#  set the motor to backwards, and reverse the direction. This is a large part
#  that accounts for the oscillation. Having a Ki will help keep the system in
#  the positive/negative region (whichever is applicable at the time) and can
#  adjust the speed without fully changing the motor to reverse -- essentially
#  it helps maintain a bit of the buffer so that the system has less oscillation.
#  Another reason that aludes to the oscillation is the frequency at which the
#  controller driver is being called. If it's called faster, it will have more
#  of a chance to adjust and correct the behavior of the motor with a finer
#  resolution. However, due to memory constraints on the Nucleo, it was hard
#  to get a much finer resolution than is seen in the figures.
#  
#  
#  @image      html 0.07_1.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 1.** Speed and Position Response with Kp = 0.07 </CENTER> \n
#  
#  @image      html 0.05_1.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 2.** Speed and Position Response with Kp = 0.05 </CENTER> \n
#  
#  @image      html 0.0425_1.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 3.** Speed and Position Response with Kp = 0.0425 </CENTER> \n
#  
#  As can be seen in the plots, lowering Kp helped the motor zero onto the 
#  desired speed. However, as can be seen in the following figure, lowering 
#  Kp too drastically had negative results.
#  
#  @image      html 0.03_2.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 4.** Speed and Position Response with Kp = 0.03 </CENTER> \n
#  
#  
#  Below is a summary of the state diagrams utilized for the week. These stayed
#  fairly consistent between weeks (the overall flow stayed the same, we were
#  just exporting/sending more data with each week).
#  
#  @image      html Front_End_FSM_Wk3.jpg " " width = 10%
#  @details    \n <CENTER> **Figure 5.** Week 3 Deliverable: Front End State Diagram </CENTER> \n
#  
#  @image      html UI_Task_FSM_Wk3.jpg " " width = 20%
#  @details    \n <CENTER> **Figure 6.** Week 3 Deliverable: UI Task State Diagram </CENTER> \n
#  
#  @image      html Ctrl_Task_FSM_Wk3.jpg " " width = 30%
#  @details    \n <CENTER> **Figure 7.** Week 3 Deliverable: Controller Task State Diagram </CENTER> \n
# 
#  
##############################################################################
## @page Week4 Week 4: Implementing a PI Controller for Tracking Purposes
#  
#  The change between week 3 and 4 was nothing too drastic, but had some key
#  differences. First, a PI controller was implemented. This only changed the
#  code by simultaneously allowing the controller driver to take in speed and
#  position, and then using a Ki value, could find the new duty cycle.
#  
#  The more interesting change that was implemented during Week 4 was the use
#  of a reference csv file that contained expected speed and position values
#  at discrete points in time. This allowed us to try speed and positon 
#  tracking with our PI controller. in PC_FrontEnd.py, the data from the csv
#  file was opened and resampled with a lower resolution. Since we aren't 
#  running the controller task at a 1ms interval, there was no need to use
#  the fine resolution from the csv file. This also helped reduce the file 
#  size that was later loaded onto the Nucleo board. Once the file was on the
#  Nucleo, the ctrlTask.py file would initialize by loading in all the data as
#  'reference values.' Each time the controller driver was called to find a 
#  new duty cycle, it would compare the current motor speeds and position 
#  against the reference data, and choose a new L value (duty cycle) 
#  accordingly. In order to find a reasonable reference time (since the file
#  has a lower resolution), an interpolation method was created in order to 
#  find an appropriate reference speed and position for the current time value.
#  This method is contained within the ctrlTask.py file.
#  
#  Similar to Week3, the following files were implemented in order to properly 
#  control the motors and encoders.
#  
#  --PC_FrontEnd.py 
#  
#  --main.py
#  
#  --uiTask.py
#  
#  --ctrlTask.py
#  
#  --encoder.encoder
#  
#  --controllerdriver.controllerdriver
#  
#  --motor.motorDriver
#  
#  --shares.py
#  
#  After the implementation of the classes and new controller setup was done,
#  a few hours of iterating Kp and Ki was needed in order to attempt in 
#  finding a combination that allowed for the motor to accurately track the
#  given profile. However, similar to week 3, the resolution of the data
#  collection arose to be the leading cause in innaccurate tracking. Adjusting 
#  Ki and Kp definitely helped with reducing the error in the system, but if
#  there was a way to get a finer resolution (say 5ms, instead of 20ms), the 
#  controller would correct and properly adjust the motor's trajectory to find
#  the next duty cycle faster. Unfortunately, due to memory allocation failures
#  on the Nucleo board, 20ms was the fastest period that could be run. Below 
#  are a few figures that show the tuning of the system, with the red line
#  shown as the given profile. Kp and Ki were derived in a MATLAB script, as
#  shown in the figure below, but due to assumptions about the friction in the
#  system and unknown inertias, the values weren't much of a help.
#  
#  @image      html Matlab_Kp_1.jpg " " width = 40%
#  @image      html Matlab_Kp_2.jpg " " width = 40%
#  @image      html Matlab_Kp_3.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 1.** Matlab Script to Derive Kp and Ki. </CENTER> \n
#  
#  Additionally, and as can be seen on the plots, 
#  there is a J metric, which is meant to display the error in the system. This
#  was described in the lab manual as the sum of the squares of the differences
#  in expected and experienced speed/position. Because the units of speed and 
#  position aren't the same, and have completel difference magnitudes, the J
#  metric is more weighted toward the error in the speed values, which is the
#  reason that it's so high, while the position tracking worked fairly well.
#  
#  @image      html Figure_5.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 2.** Speed and Position Response with Kp = 0.05 and Ki = 0.06 </CENTER> \n
#  
#  @image      html Figure_6.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 3.** Speed and Position Response with Kp = 0.05 and Ki = 0.05 </CENTER> \n
#  
#  @image      html Figure_8.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 4.** Speed and Position Response with Kp = 0.0375 and Ki = 0.04 </CENTER> \n
#  
#  @image      html Figure_7.jpg " " width = 40%
#  @details    \n <CENTER> **Figure 5.** Speed and Position Response with Kp = 0.035 and Ki = 0.04 </CENTER> \n
#  
# 
# 
#  Below is a summary of the state diagrams utilized for the week. These stayed
#  fairly consistent between weeks (the overall flow stayed the same, we were
#  just exporting/sending more data with each week).
#  
#  @image      html Front_End_FSM.jpg " " width = 10%
#  @details    \n <CENTER> **Figure 8.** Week 4 Deliverable: Front End State Diagram </CENTER> \n
#  
#  @image      html UI_Task_FSM.jpg " " width = 20%
#  @details    \n <CENTER> **Figure 9.** Week 4 Deliverable: UI Task State Diagram </CENTER> \n
#  
#  @image      html Ctrl_Task_FSM.jpg " " width = 30%
#  @details    \n <CENTER> **Figure 10.** Week 4 Deliverable: Controller Task State Diagram </CENTER> \n
# 