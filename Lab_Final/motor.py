'''
@file       motor.py
@brief      Set the motors to run with a specified duty cycle.
@details    The controller driver will calculate a new duty cycle and input
            into this class, where it will set the according motor to the new
            duty cycle. Other methods include enabling and disabling the 
            nSLEEP pin, which is allows the motor to be turned on or off.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/motor.py
@author     Matthew Frost
@date       Originally created on 02/25/21 \n Last modified on 03/17/21
'''
from pyb import Timer, Pin
import math

class motorDriver:
    ''' 
     @brief    Motor class to allow for several motors to be used.
     @details  This class will allow for the enabling and disabling of the 
               motors, as well as the ability to set the motors to a specified
               duty cycle through PWM. 
    '''
    def __init__ (self, debugFlag, nSLEEP_pin, IN1_pin, IN2_pin, timer, ch1, ch2):
        ''' 
        @brief   Creates a motor driver by initializing GPIO pins.
        @details The user will have to input the pins for the motor and the 
                 potential timers and channels as specified in the 
                 manufacturers specifications for the specific motor.
        @param   debugFlag Enables or prevents certain print statements from
                 appearing.
        @param   nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param   IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param   IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param   timer A pyb.Timer object to use for PWM generation on
                 IN1_pin and IN2_pin. An example is: Timer(3, freq=20000).
        @param   ch1 A channel that will be used for the timer. This must be 
                 consistent with the channel options for the specific motor.
        @param   ch2 A channel that will be used for the timer. This must be 
                 consistent with the channel options for the specific motor.         
        '''
        ## @brief       Debugging flag for detailed analysis while running
        self.debugFlag  = debugFlag 
        ## @brief       Used to enable or disable the motor
        self.nSLEEP_pin = Pin(nSLEEP_pin, mode=Pin.OUT_PP, value=1)
        ## @brief       Object for the timer attribute
        self.timer      = timer
        ## @brief       The timer channel object for half bridge 1
        self.IN1        = self.timer.channel(ch1, mode=Timer.PWM, pin=IN1_pin)
        ## @brief       The timer channel object for half bridge 2
        self.IN2        = self.timer.channel(ch2, mode=Timer.PWM, pin=IN2_pin)
        
    def enable (self):
        '''
        @brief  Enable the motor to allow for PWM to actuate the motor.
        '''
        print ('Enabling Motor')
        self.nSLEEP_pin.high()
        
    def disable (self):
        '''
        @brief  Disable the motor to prevent the PWM to actuate the motor.
        '''
        print ('Disabling Motor')
        self.nSLEEP_pin.low()
        
    def setDuty (self, duty):
        ''' 
        @brief    Set the duty of the motors.
        @details  This method sets the duty cycle to be sent to the motor to the 
                  given level. Positive values cause effort in one direction, negative 
                  values in the opposite direction.
        @param    duty A signed integer holding the duty cycle of the PWM signal 
                  sent to the motor. 
        '''
        # If the incoming duty is greater than 100 or less than -100, 
        # auto-set the duty to 100 or -100, respectively, since it's not 
        # possible to be larger than 100%
        if duty > 100:
            duty = 100
            if self.debugFlag == True: 
                print('MOTTASK: Invalid Response, setting to 100')
        elif duty < -100:
            duty = -100
            if self.debugFlag == True: 
                print('MOTTASK: Invalid Response, setting to -100')
            
        # Set the duty cycle of the motor to the appropriate direction and value
        if 100 >= duty >= 0:
            if self.debugFlag == True: 
                print('MOTTASK: Setting motor to go forward with a duty cycle of: ' + str(duty))
            self.IN1.pulse_width_percent(duty)
            self.IN2.pulse_width_percent(0)
        elif -100 <= duty < 0:
            if self.debugFlag == True: 
                print('MOTTASK: Setting motor to go backwards with a duty cycle of: ' + str(duty))
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(math.fabs(duty))
        else:
            if self.debugFlag == True: 
                print('MOTTASK: Setting motor to not move with a duty cycle of: ' + str(duty))
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(0)