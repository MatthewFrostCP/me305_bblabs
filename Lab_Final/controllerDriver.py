'''
@file       controllerDriver.py
@brief      Implement a PI controller for speed and position tracking.
@details    Using the derived values from in class and MATLAB, the proportional
            and integral gains will be utilized to predict the next duty cycle
            of the motor. This is how the motor can follow a profile.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/controllerDriver.py
@author     Matthew Frost
@date       Originally created on 03/10/21 \n Last modified on 03/17/21 
'''

class controllerDriver:
    ''' 
     @brief    Controller class to allow for positional tracking.
     @details  This class will allow for motor control using a PI controller
               method. 
    '''
    def __init__ (self, debugFlag = True, Kp = 0.05, Ki = 0.04):
        ''' 
        @brief   Creates a controller driver.
        @details The user will have to specify the specific gains, Kp and Ki,
                 and whether they want to see the debugging print statements.
        @param   debugFlag Enables or prevents certain print statements from
                 appearing.
        @param   Kp The proportional gain. 
        @param   Ki The integrating gain.        
        '''
        ## @brief       Debugging flag for detailed analysis while running
        self.debugFlag  = debugFlag 
        ## @brief       Proportional gain, in units of %/RPM
        self.KpPrimeRPM = Kp
        ## @brief       Proportional gain, in units of %/deg
        self.KiPrimeRPM = Ki
        
    def update(self, omegaRef, omegaCurrent, thetaRef, thetaCurrent):
        ''' 
        @brief    Update the Controller Task with a new duty cycle for the motor.
        @details  Using a PI algorithm, take the reference speed and position,
                  compare it to the current speed and position, and calculate
                  the necessary duty to get back on track.
        @param    omegaRef The expected speed at a specific instance in time.
        @param    omegaCurrent The current speed at a specific instance in time.
        @param    thetaRef The expected position at a specific instance in time.
        @param    thetaCurrent The currrent position at a specific instance in time.
        @return   The new duty cycle to be applied to the motor.
        '''
        if self.debugFlag == True:
            print('CTRLDVR: Incoming omegaCurrent is: ' + str(omegaCurrent) + ' rpm')
        
        ## @brief  The difference in speed in RPM at a specific instance
        omegaDifference = omegaRef - omegaCurrent
        ## @brief  The difference in position in degrees at a specific instance
        thetaDifference = thetaRef - thetaCurrent
        
        ## @brief  The new duty cycle based on the differences in speed and position.
        self.newL = self.KpPrimeRPM*(omegaDifference) + self.KiPrimeRPM*(thetaDifference)
        
        if self.debugFlag == True: 
            print('CTRLDVR: Setting the new L to: ' + str(self.newL))
        
        return self.newL