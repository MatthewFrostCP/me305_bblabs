# -*- coding: utf-8 -*-
'''
@file       main.py
@brief      Create objects for the UI Task and Controller Task
@details    This is named main.py so that it automatically runs when the Nucleo
            is reset. All it does is creates and runs the UI and Controller Tasks.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/main.py
@author     Matthew Frost
@date       Originally created on 02/18/21 \n Last modified on 03/17/21
'''
from ctrlTask import ctrlTask
from uiTask import uiTask

if __name__ == "__main__":
    
    ## @brief An instance of the Ui Task
    task1 = uiTask(True, uiPeriod = 15)
    ## @brief An instance of the Controller Task
    task2 = ctrlTask(True, ctrlPeriod = 20)   
  
    while True: 
        try: 
            # Run the tasks continuously
            task1.run() 
            task2.run()
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization
    print('Thanks for testing out the program. See you next time!')
