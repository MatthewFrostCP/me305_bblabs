'''
@file       encoder.py
@brief      In charge of keeping track of the position and speed of encoder.
@details    This script implements an algorithm that accounts for overflow and
            underflow in the encoder tick values. From this, the position, in 
            degrees, and speed, in rpm, can be computed and sent back to the
            controller task. Other features are instantaneously getting the 
            position, speed, and delta, as well as zeroing the encoder.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/encoder.py
@author     Matthew Frost
@date       Originally created on 02/25/21 \n Last modified on 03/17/21
'''
from pyb import Timer, Pin
import utime

class encoder:
    ''' 
     @brief    Encoder class to allow for several encoders to be used.
     @details  This class will allow for the position, delta, and speed of
               the encoders to be tracked.
    '''
    def __init__(self, debugFlag = True, pin1 = Pin.cpu.B6, pin2 = Pin.cpu.B7, encTimer = 4):
        ''' 
        @brief    Create an encoder object to find position and speeds.
        @details  This object will be called in order to find the
                  current positions, deltas, speeds, or to reset the
                  encoder back to zero.
        @param    pin1 Input pin to use for the first channel. User must input
                  the exact pin name, for example, Pin.cpu.B6 for encoder 1 or
                  Pin.cpu.C6 for encoder 2.
        @param    pin2 Input pin to use for the second channel. User must input
                  the exact pin name, for example, Pin.cpu.B7 for encoder 1 or
                  Pin.cpu.C7 for encoder 2.
        @param    encTimer Timer number that will be used for the encoder. 
                  Possible options include timer 4 and 8.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
       '''
        ## @brief           Debugging flag for detailed analysis while running
        self.debugFlag      = debugFlag 
        ## @brief           The period of the counter (maximum, 0xFFFF)
        self.period         = 65535
        ## @brief           Create an object for the attribute pin1
        self.pin1           = pin1
        ## @brief           Create an object for the attribute pin2
        self.pin2           = pin2
        ## @brief           Create a timer object
        self.TIM4           = Timer(encTimer, prescaler = 0, period = self.period)
        ## @brief           The first channel for the timer
        self.tim4ch1        = self.TIM4.channel(1, mode = Timer.ENC_AB, pin = pin1)
        ## @brief           The second channel for the timer
        self.tim4ch2        = self.TIM4.channel(2, mode = Timer.ENC_AB, pin = pin2)
        ## @brief           Use the timer to get the current count for the tick
        self.currentTick    = self.TIM4.counter()
        ## @brief           Create a variable for the previous tick
        self.previousTick   = self.currentTick
        ## @brief           Converts ticks into degrees (PPR = 4000)
        self.theta          = self.currentTick*360/4000
        ## @brief           Variable to find the difference in ticks
        self.deltaTick      = self.currentTick - self.previousTick
        ## @brief           Variable to hold motor speed
        self.speed          = 0
        ## @brief           Current time for the encoder
        self.encCurrentTime = utime.ticks_ms()
        ## @brief           Previous time for the encoder
        self.encLastTime    = self.encCurrentTime
        
        
    def update(self): 
        '''
        @brief      Update the position and velocity of the encoders.
        @details    This method updates the position (in ticks and degrees) 
                    as well as the speed of the motor whenever called. It will 
                    be called by the controller task whenever the the p/P key
                    is pressed, or data is being collected.
        '''
        self.currentTick = self.TIM4.counter() # find the current tick of the encoder
        self.deltaTick = self.currentTick - self.previousTick # find delta
        if self.debugFlag == True: 
            print('ENC: Encoder is at: ' + str(self.currentTick) + ' ticks')
            print('ENC: Encoder delta is at: ' + str(self.deltaTick) + ' ticks')
        
        # algorithm to account for overflow or underflow
        if self.deltaTick > 0.5*self.period:
            self.deltaTick = self.deltaTick - self.period
        elif self.deltaTick < -0.5*self.period:
            self.deltaTick = self.deltaTick + self.period
        else:
            self.deltaTick = self.deltaTick
        
        # Compute position in degrees
        self.theta += self.deltaTick*360/4000 # degrees
        if self.debugFlag == True: 
            print('ENC: current theta value is: ' + str(self.theta) + ' deg')
        
        # Find current time 
        self.encCurrentTime = utime.ticks_ms() 
        if self.debugFlag == True: 
            print('ENC: Current time for elapsed time is: ' + str(self.encCurrentTime) + ' ms')
        
        ## @brief Find the elapsed time since the last computation
        self.encPeriod = utime.ticks_diff(self.encCurrentTime, self.encLastTime)
        if self.debugFlag == True: 
            print('ENC: Elapsed time is: ' + str(self.encPeriod) + ' ms')
        
        # Compute the speed in rpm
        self.speed = 1000*(60/4000)*(self.deltaTick/self.encPeriod) # rpm
        if self.debugFlag == True: 
            print('ENC: Speed is: ' + str(self.speed) + ' rpm')
        
        # Record the current tick for the next iteration as previousTick
        self.previousTick = self.currentTick
        if self.debugFlag == True: 
            print('ENC: setting previous tick to: ' + str(self.previousTick) + ' ticks')
        
        # Save the current time for the next iteration as encLastTime
        self.encLastTime = self.encCurrentTime
        if self.debugFlag == True: 
            print('ENC: Last time for elapsed time is: ' + str(self.encLastTime) + ' ms')
        
        
    def getPosition(self):
        '''
        @brief      Get the position of the encoder.
        @details    When called, this function will output the current angle
                    of the encoder in degrees.
        @return    The current position of the encoder.
        '''
        if self.debugFlag == True: 
            print('ENC: sending CtrlTask the current position of: ' + str(self.theta))
        return self.theta 
        
    
    def setPosition(self, newPosition):
        '''
        @brief      Zero the encoder.
        @details    When called, this will set theta to 0, thus resetting the
                    encoder.
        @return     The new angle for the encoder.
        '''
        self.theta = newPosition
        if self.debugFlag == True: 
            print('ENC: Position has been set to: ' + str(self.theta))
        return self.theta
    
    
    def getSpeed(self):
        '''
        @brief      Get the speed of the encoder.
        @details    When called, this function will output the current speed
                    of the encoder in rpm.
        @return     The current speed of the encoder.
        '''
        if self.debugFlag == True: 
            print('ENC: sending CtrlTask the current speed of: ' + str(self.speed))
        return self.speed 
        
    
    def getDelta(self):
        '''
        @brief      Get the delta of the encoder.
        @details    When called, this function will output the current delta
                    of the encoder in ticks.
        @return     The current deslta of the encoder.
        '''
        if self.debugFlag == True:     
            print('ENC: sending CtrlTask the current delta of: ' + str(self.deltaTick))
        return self.deltaTick