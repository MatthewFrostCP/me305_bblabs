# -*- coding: utf-8 -*-
'''
@file       PC_FrontEnd.py
@brief      Set up UI between Spyder and Nucleo to collect and plot data.
@details    Use the serial port to send and receive data between Spyder console
            and Nucleo. Nucleo, when told to, will take data according to a 
            specific function, and when prompted or timed out, will send the data
            back to Spyder to be plotted.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/PC_FrontEnd.py
@image      html Front_End_FSM.jpg " " width = 10%
@details    \n <CENTER> **Figure 1.** Task Diagram for Front End </CENTER> \n
@author     Matthew Frost
@date       Originally created on 02/18/21 \n Last modified on 03/17/21
'''
import serial
import csv
import keyboard
import os 
from matplotlib import pyplot as plt
import numpy as np

## @brief Set up serial communication through serial command
ser = serial.Serial(port='COM5', baudrate=115273, timeout=1)
ser.flushInput()

def sendChar(last_key):
    ''' 
    @brief   Sends a string through serial to Nucleo.
    @details When keyboard is triggered, encode that letter and send it through 
             serial to the Nucleo to read.
    @param   last_key The current key that was pressed.     
    '''
    ser.write(str(last_key).encode('ascii'))
    print('FrontEnd: You have sent a letter')

def kb_cb(key):
    ''' 
    @brief   Callback function inside keyboard module.
    @details When keyboard is pressed, it automatically gets sent to the
             callback function to assign a variable the letter pressed. 
    @param   key The key that was pressed to trigger the callback function.
    '''
    global last_key
    last_key = key.name
    
def writeToCSV(data, filename):
    ''' 
    @brief   Strip and split the data into a csv file from the Nucleo.
    @details After data collection is finished, the data must be stripped
             and split in order to write to a csv file.
    @param   data The data from the serial port that needs to be saved into the CSV.
    @param   filename The specific file to save the data to.                 
    '''
    dataStripped = data.strip()
    print('Front end: dataStripped is currently: ' + str(dataStripped))
    dataSplit    = dataStripped.split(', ')
    
    timeData  = float(dataSplit[0])
    print('Front end: current timeData is: ' + str(timeData))
    enc1AngleData = float(dataSplit[1])
    enc1SpeedData = float(dataSplit[2])
    enc2AngleData = float(dataSplit[3])
    enc2SpeedData = float(dataSplit[4])
                          
    with open(filename,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([timeData, enc1AngleData, enc1SpeedData, enc2AngleData, 
                         enc2SpeedData])
        
def plotData(csvFileName, col1, col2,  y1label, y2label, plotTitle, fileName, Kp, Ki, J):
    ''' 
    @brief   Plot the position and speed data from the encoders.
    @details The subplot feature within matlibplot will be utilized to visualize
             the position and speed of the motors along with the reference 
             data to see the difference in position and speed at all points in
             time.
    @param   csvFileName Specifies the file to grab the data from.
    @param   col1 Speciifes which column of data to plot on the x axis.
    @param   col2 Speciifes which column of data to plot on the y axis.
    @param   y1label Specifies the vertical label for the first subplot. 
    @param   y2label Specifies the vertical label for the second subplot. 
    @param   plotTitle Specifies a plot title for the figure.
    @param   fileName Specifies the name of what to save the figure to.
    @param   Kp The proportional gain used in testing that is currently being plotted. 
    @param   Ki The integral gain used in testing that is currently being plotted.  
    @param   J The error metric of the tracking precision currently being plotted.         
    '''
    x1, y1 = np.loadtxt(csvFileName, delimiter=',', usecols=(0, col1), unpack=True)
    x2, y2 = np.loadtxt(csvFileName, delimiter=',', usecols=(0, col2), unpack=True)
    tRef, thetaRef = np.loadtxt("sampledRefCSV.csv", delimiter=',', usecols=(0, 2), unpack=True)
    tRef, omegaRef = np.loadtxt("sampledRefCSV.csv", delimiter=',', usecols=(0, 1), unpack=True)
    font = {'fontname':'Times New Roman'}
    plt.subplot(211)
    plt.plot(x1,y1, color='black')
    plt.plot(tRef, thetaRef, color='red')
    plt.ylabel(y1label,**font)
    plt.title(plotTitle,**font)
    plt.legend(loc ="lower right", prop={'family':'Times New Roman'})
    plt.text(5,1500,'Kp = {:}, Ki = {:}, J = {:}'.format(Kp, Ki, J),**font)
    plt.xlim(min(x1),max(x1))
    plt.ylim(min(y1)-50,max(y1)+500)
    
    plt.subplot(212)
    plt.plot(x2,y2, color='black')
    plt.plot(tRef, omegaRef, color='red')
    plt.xlabel('Time, t, [ sec ]',**font)
    plt.ylabel(y2label,**font)
    plt.legend(prop={'family':'Times New Roman'})
    plt.xlim(min(x2),max(x2))
    plt.ylim(min(y2)-50,max(y2)+100)
    
    plt.savefig(fileName)
    plt.show()
    
def sampleCSV(refCSV, ctrlPeriod, filename):
    ''' 
    @brief   Sample the reference data at a lower resolution.
    @details Since the data cannot be loaded entirely onto the Nucleo for 
             memory reasons, it must be sampled at a specific rate. This allows 
             for the high-resolution data to be reduced to a more reasonable
             resolution, as specified by the ctrlPeriod.
    @param   refCSV Filename for the reference csv file of data.
    @param   ctrlPeriod The frequency that the data should be sampled.
    @param   filename The specific file to save the data to.                 
    '''
    ## @brief  The counter for sampling
    sampleN = 0
    ## @brief  The number of rows that should be saved
    maxNum  = 15000/ctrlPeriod
    ## @brief  A list for the reference time values
    timeRef = []
    ## @brief  A list for the reference position values
    omegaRef = []
    ## @brief  A list for the reference speed values
    thetaRef = []
    
    for sampleN in range(int(maxNum)):
        curTimeRef, curOmegaRef, curThetaRef = np.loadtxt(refCSV, delimiter=',', 
                                                  unpack = True, skiprows = sampleN*25,
                                                  max_rows = 1)
        timeRef.append(curTimeRef)
        omegaRef.append(curOmegaRef)
        thetaRef.append(curThetaRef)
        sampleN += 1
    
        with open(filename,"a") as f:
            writer = csv.writer(f,delimiter=",")
            writer.writerow([curTimeRef, curOmegaRef, curThetaRef])
    
    return timeRef, omegaRef, thetaRef
    
## @brief keeps track of if the intro message should be displayed
introMessage = True

## @brief holds the value of the current keyboard letter
last_key = None
## @brief          File name of reference data
refCSV             = "reference.csv"
## @brief          File name of sampled reference data
sampledRefCSV      = "sampledRefCSV.csv"
## @brief          File name of the exported data
csvFileNameTotal   = "LabFinalData.csv"

# check to see if the csv file exists, and if it does, delete it so it can be
# written over with new data
if os.path.exists(csvFileNameTotal):
    os.remove(csvFileNameTotal)

## @brief          File name fo Encoder 1
pdfFileNameEnc1    = "Enc1Plot.pdf"
## @brief          File name fo Encoder 2
pdfFileNameEnc2    = "Enc2Plot.pdf"

## @brief          Vertical axis label for Encoder 1 Position
yLabelEnc1Position = 'Encoder 1 Position'
## @brief          Vertical axis label for Encoder 1 Speed
yLabelEnc2Position = 'Encoder 2 Position'
## @brief          Vertical axis label for Encoder 2 Position
yLabelMot1Speed    = 'Motor 1 Speed'
## @brief          Vertical axis label for Encoder 2 Speed
yLabelMot2Speed    = 'Motor 2 Speed'
## @brief          Title of Encoder 1 plot
titleEnc1Data      = 'Encoder 1 Data'
## @brief          Title of Encoder 2 plot
titleEnc2Data      = 'Encoder 2 Data'

# Predefine a list of keys that the keyboard will respond to.
## @brief  list the specific keys used in the front end
Keys       = ["s","g","p","d","z","m","v","b","P","D","Z","M","V","B"] 
    
for m in range(len(Keys)):
    keyboard.on_release_key(Keys[m], callback = kb_cb)

## @brief      Small FSM state variable within the front end
frontEndState = 0

## @brief      The outputted data from the serial port
dataFromNucleo = None

# Create instances of variables to be plotted later
## @brief  Kp value used in the controller driver
Kp         = 0
## @brief  Kp value used in the controller driver
Ki         = 0
## @brief  J metric for Encoder 1
J1         = 0
## @brief  J metric for Encoder 2
J2         = 0

# if data needs to be sampled again, uncomment this code and the method will run
# timeRef, omegaRef, thetaRef = sampleCSV(refCSV, 25, sampledRefCSV)

while True:
    try:      
        
        if frontEndState == 0:
            if introMessage == True:
                # prompt the user if they would like to start/stop data collection
                print('Front End: To zero the encoder, press z.'
                      ' To see the current position, press p.'
                      ' To see the current delta value, press d.'
                      ' To sample data for 30 seconds, press g.'
                      ' To stop data collection, press s.'
                      ' To start a motor, press m.'
                      ' To disable a motor, press b.'
                      ' To get the velocity, press v.'
                      ' For utilizing the second motor, capitalize the commands.')
                introMessage = False
                
            if last_key is not None:
                print("FrontEnd: You pressed " + last_key)
                sendChar(last_key)
                # read data from the uart port on the Nucleo after each loop
                dataFromNucleo = ser.readline().decode('ascii')
                if last_key == 's':
                    introMessage = False
                    frontEndState = 1
                    print('FrontEnd: State set to: ' + str(frontEndState))
                else:
                    introMessage = True
                last_key = None
                
            if dataFromNucleo is not None:
                print(dataFromNucleo)  
                dataFromNucleo = None

        elif frontEndState == 1:
            # read data from the uart port on the Nucleo after each loop
            dataFromNucleo = ser.readline().decode('ascii')

            if dataFromNucleo:

                if dataFromNucleo[0] == 'J':
                    # Take out the Kp, Ki, J1, and J2 variables and store
                    ## @brief  The J metrics from the controller task
                    Jvalues = dataFromNucleo[1:]
                    Jvalues = Jvalues.strip()       
                    Jvalues = Jvalues.split(',') 
                    Kp = float(Jvalues[0])
                    Ki = float(Jvalues[1])
                    J1 = float(Jvalues[2])
                    J2 = float(Jvalues[3])
                
                elif dataFromNucleo == 'generatePlot\r\n':  
                    # send command to plot all the data in the csv file
                    plotData(csvFileNameTotal, 1, 2, yLabelEnc1Position,
                             yLabelMot1Speed, titleEnc1Data, pdfFileNameEnc1,
                             Kp, Ki, J1)
                    plotData(csvFileNameTotal, 3, 4, yLabelEnc2Position,
                             yLabelMot2Speed, titleEnc2Data, pdfFileNameEnc2,
                             Kp, Ki, J2)

                else:
                    print('Front end: Currently writing ' + str(dataFromNucleo) + ' to CSV')
                    writeToCSV(dataFromNucleo, csvFileNameTotal)


    except KeyboardInterrupt:
        keyboard.unhook_all()
        break

# Turn off the callbacks so next time we run this behave as expected
keyboard.unhook_all()

# closer serial port
ser.close()