# -*- coding: utf-8 -*-
'''
@file       ctrlTask.py
@brief      This file interfaces with the encoder, motor, and controller tasks
@details    When a key is pressed, a command gets sent to shares.py, and read
            in this file. Based on the input command, certain tasks will be 
            executed, such as getting the current position or speed of the 
            encoders. These are then sent back through shares.py to the UI task
            to send through serial to the front end.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/ctrlTask.py
@image      html Ctrl_Task_FSM.jpg " " width = 40%
@details    \n <CENTER> **Figure 1.** Task Diagram for Controller Task </CENTER> \n
@author     Matthew Frost
@date       Originally created on 03/02/21 \n Last modified on 03/17/21
'''
import pyb
import utime
import shares
from encoder import encoder
from pyb import Timer, Pin
from motor import motorDriver
from controllerDriver import controllerDriver
from array import array
import gc

# Create a motor object passing in the pins and timer
## @brief  An instance of the Motor Driver for Motor 1
mot1 = motorDriver(True, Pin.cpu.A15, Pin.cpu.B4, Pin.cpu.B5, Timer(3, freq=20000), 1, 2)
## @brief  An instance of the Motor Driver for Motor 2
mot2 = motorDriver(True, Pin.cpu.A15, Pin.cpu.B0, Pin.cpu.B1, Timer(3, freq=20000), 3, 4)

# create an encoder object to pass in pins and timer
## @brief  An instance of the Encoder Driver for Encoder 1
enc1 = encoder(False, pin1 = pyb.Pin.cpu.B6, pin2 = pyb.Pin.cpu.B7, encTimer = 4)  
## @brief  An instance of the Encoder Driver for Encoder 2
enc2 = encoder(False, pin1 = pyb.Pin.cpu.C6, pin2 = pyb.Pin.cpu.C7, encTimer = 8)  

# Input a guess for Kp and Ki
## @brief  The Kp value
shares.sharedKpValue = 0.035
## @brief  The Ki value
shares.sharedKiValue = 0.041

# create an object for the controllerDriver
## @brief  An instance of the Controller Driver for controlling the motors
ctrlDriver = controllerDriver(Kp = shares.sharedKpValue, Ki = shares.sharedKiValue )

class ctrlTask:
    ''' 
     @brief    Controller Task to interface with encoder, motor, and controller
     @details  This class will allow for the encoder and motors to be controlled
               by the controler driver. It will interface with shares.py for 
               the UI task to read from.
    '''
    ## @brief         Samples data at a lower resolution that given in reference.csv
    S0_SAMPLE_DATA    = 0   
    ## @brief         Basic init state, wait for a command to come in
    S1_WAIT_FOR_INPUT = 1   
    ## @brief         Read any data if certain letters are pressed
    S2_READ_DATA      = 2   
    ## @brief         Run the motor controller tasks to track position and velocity
    S3_CTRL_RUN       = 3   
    ## @brief         A finished state for when the stop command is received
    S4_FINISHED       = 4   

    def __init__(self, debugFlag = True, ctrlPeriod = 25):
        ''' 
        @brief    Initialize the controller task.
        @details  This task will interface with the encoders, motors, and 
                  motor controller. It will write to shares.py for the UI task
                  to read and write to the Spyder Console.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        @param    ctrlPeriod Specifies the rate at which the controller task is
                  called, in milliseconds. This must be slower than the UI
                  task, or else the UI task will miss data points.
       '''
        ## @brief        Contains the information for which state its in   
        self.state       = 0 # start in the sample data state
        ## @brief        Debugging flag for detailed analysis while running
        self.debugFlag   = debugFlag # used to view debugging print statements
        ## @brief        Uses the utime module to keep track of time
        self.currentTime = utime.ticks_ms()
        ## @brief        Defines the Controller Task period in ms
        self.ctrlPeriod  = ctrlPeriod
        ## @brief        Time stamp for the next iteration of the task
        self.nextTime    = utime.ticks_add(self.currentTime, self.ctrlPeriod)
        ## @brief        Placeholder for the new duty cycle for Motor 1
        self.newDutyMot1 = 0
        ## @brief        Placeholder for the new duty cycle for Motor 2
        self.newDutyMot2 = 0
        ## @brief        The max time before program times out in ms
        #  @details      This time is based on the given speed and position
        #                curves for controller tracking.
        self.maxTime     = 15000
        ## @brief        Defines the start time (used for interpolating purposes)
        #  @details      When the g command is pressed, it will record that
        #                specific time. From then on out, any elapsed time will 
        #                be referenced back to startTime in order to see how 
        #                long the motor has been running for
        self.startTime   = self.currentTime
        ## @brief        Keeps track of the time since startTime
        self.elapsedTime = utime.ticks_diff(self.currentTime, self.startTime)
        ## @brief        Counter for the interpolation function
        self.currentI    = 0
        gc.collect()
        ## @brief        Array for the reference time data to track
        #  @details      This data is provided in the lab manual and will be 
        #                pre-sampled in to a less-fine resolution that is more
        #                applicable to the data collection speed run here
        self.timeRef     = array('f', int(self.maxTime/self.ctrlPeriod)*[0])
        gc.collect()
        ## @brief        Array for the reference speed data to track
        #  @details      This data is provided in the lab manual and will be 
        #                pre-sampled in to a less-fine resolution that is more
        #                applicable to the data collection speed run here
        self.omegaRef    = array('f', self.timeRef)
        gc.collect()
        ## @brief        Array for the reference position data to track
        #  @details      This data is provided in the lab manual and will be 
        #                pre-sampled in to a less-fine resolution that is more
        #                applicable to the data collection speed run here
        self.thetaRef    = array('f', self.timeRef)
        gc.collect()
        ## @brief        Tracks the J parameter to measure the precision of Enc 1
        self.J1          = 0
        ## @brief        Tracks the J parameter to measure the precision of Enc 2
        self.J2          = 0
       
    def run(self): 
        '''
        @brief      Report necessary measurements to the shares.py task.
        @details    This method updates with the position (in degrees) of the
                    encoders as well as the speeds of the motor whenever 
                    requested. It will read any input commands from the UI task
                    and write to shares.py when necessary.
        '''
        self.currentTime = utime.ticks_ms() # compute new time
        
        # Establish a period at which the controller task is ran.
        if utime.ticks_diff(self.currentTime, self.nextTime) >= 0:
            # time stamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.ctrlPeriod)
            
            # update the encoders as rapidly as possible
            enc1.update()
            enc2.update()
            
            if self.state == self.S0_SAMPLE_DATA:
                # read the sampled data and load it into the pre-allocated arrays
                self.readCSV('sampledRefCSV.csv')
                self.transitionTo(self.S1_WAIT_FOR_INPUT)    
            
            if self.state == self.S1_WAIT_FOR_INPUT:
                if self.debugFlag == True:
                    print('CtrlTask: Initializing program')
                if shares.sharedStartCmd == 'Sending a character':
                    shares.sharedStartCmd = None # reset variable
                    if shares.sharedStartCtrl == True:
                        print('CtrlTask: Receiving sharedStartCmd at: ' + str(self.currentTime) + ' ms')
                        self.transitionTo(self.S3_CTRL_RUN)
                        self.startTime = self.currentTime  # record time
                        print('CtrlTask: Setting startTime to: ' + str(self.startTime) + ' ms')
                        shares.sharedStartCtrl = False # reset variable
                    else:
                        self.transitionTo(self.S2_READ_DATA)
                
            elif self.state == self.S2_READ_DATA:
                
                # Commands for encoder 1
                if shares.sharedCmd1 == 'p':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a p. Getting Enc 1 positon now.')
                    shares.sharedPositionEnc1 = enc1.getPosition() # Write position to shares.py
                elif shares.sharedCmd1 == 'd':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a d. Getting Enc 1 delta now.')
                    shares.sharedDeltaEnc1 = enc1.getDelta() # Write delta to shares.py
                elif shares.sharedCmd1 == 'z':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a z. Zeroing encoder Enc 1 now.')
                    shares.sharedPositionEnc1 = enc1.setPosition(0) # Reset the encoder to 0
                elif shares.sharedCmd1 == 'm':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a m. Setting motor 1 to turn on.')
                    mot1.enable()
                    mot1.setDuty(50) # Set motor to arbitrary duty cycle
                    shares.sharedStartCmd = 'Sending a character' # reset to continue motor
                elif shares.sharedCmd1 == 'v':
                    if self.debugFlag == True:    
                        print('CtrlTask: You have pressed a v. Getting Mot 1 speed now.')
                    shares.sharedSpeedMot1 = enc1.getSpeed() # Write speed to shares.py
                elif shares.sharedCmd1 == 'b':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a b. Stopping motor 1.')
                    mot1.disable()
                    mot1.setDuty(0) # Turn off motor
                
                elif shares.sharedCmd2 == 'P':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a P. Getting current Enc 2 positon now.')
                    shares.sharedPositionEnc2 = enc2.getPosition() # Write position to shares.py
                elif shares.sharedCmd2 == 'D':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a D. Getting current Enc 2 delta now.')
                    shares.sharedDeltaEnc2 = enc2.getDelta() # Write delta to shares.py
                elif shares.sharedCmd2 == 'Z':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a Z. Zeroing encoder Enc 2 position now.')
                    shares.sharedPositionEnc2 = enc2.setPosition(0) # Reset the encoder to 0
                elif shares.sharedCmd2 == 'M':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a M. Setting motor 2 to turn on.')
                    mot2.enable()
                    mot2.setDuty(80) # Set motor to arbitrary duty cycle
                    shares.sharedStartCmd = 'Sending a character' # reset to continue motor
                elif shares.sharedCmd1 == 'V':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a V. Getting Mot 2 speed now.')
                    shares.sharedSpeedMot2 = enc2.getSpeed() # Write speed to shares.py
                elif shares.sharedCmd1 == 'B':
                    if self.debugFlag == True:
                        print('CtrlTask: You have pressed a B. Stopping motor 2.')
                    mot2.disable()
                    mot2.setDuty(0) # Turn off motor
                else:
                    pass
                
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
                
            elif self.state == self.S4_FINISHED:
                pass
            
            elif self.state == self.S3_CTRL_RUN:
                print('CtrlTask: State 3 has been reached at: ' + str(self.currentTime) + ' ms')
                if shares.sharedCmd1 == 's':
                    self.transitionTo(self.S4_FINISHED)
                elif shares.sharedCmd1 == 'b':
                    mot1.disable()
                    mot2.disable()
                else: 
                    
                    # Interpolate to find the new reference position and speed
                    newTheta, newSpeed = self.getCurrentRef(self.startTime, self.currentTime, self.currentI)
                    if self.debugFlag == True: 
                        print('CtrlTask: Getting new position of: ' + str(newTheta) + ' and speed of: ' + str(newSpeed))
                    
                    # Get current omegaRef and thetaRef
                    ## @brief  The reference omega of the current loop for Motor 1
                    self.omegaRefMot1 = newSpeed # rpm
                    ## @brief  The reference omega of the current loop for Motor 2
                    self.omegaRefMot2 = newSpeed # rpm
                    ## @brief  The reference position of the current loop for Encoder 1
                    self.thetaRefEnc1 = newTheta  # degrees
                    ## @brief  The reference position of the current loop for Encoder 1
                    self.thetaRefEnc2 = newTheta  # degrees
                    
                    # For exporting data, get the current positions/speeds of the encoder
                    shares.sharedPositionEnc1 = enc1.getPosition()
                    shares.sharedPositionEnc2 = enc2.getPosition()
                    shares.sharedSpeedMot1 = enc1.getSpeed()
                    shares.sharedSpeedMot2 = enc2.getSpeed()
                    
                    # Find the differences in positions/speeds in order to calculate J
                    thetaDiffEnc1 = self.thetaRefEnc1 - shares.sharedPositionEnc1
                    thetaDiffEnc2 = self.thetaRefEnc2 - shares.sharedPositionEnc2
                    omegaDiffMot1 = self.omegaRefMot1 - shares.sharedSpeedMot1
                    omegaDiffMot2 = self.omegaRefMot2 - shares.sharedSpeedMot2
                    
                    # Compute the new J value by adding to the previous ones
                    self.J1 += (omegaDiffMot1)**2 + (thetaDiffEnc1)**2
                    self.J2 += (omegaDiffMot2)**2 + (thetaDiffEnc2)**2
                    
                    if self.debugFlag == True:
                        print('J1: {:}, J2: {:}'.format(self.J1, self.J2))
                    
                    # Write the J values to the shares.py file and divide by
                    # the total number of iterations
                    shares.sharedJ1 = self.J1/len(self.timeRef)
                    shares.sharedJ2 = self.J2/len(self.timeRef)
                    
                    # Figure out the new L for each motor
                    self.newDutyMot1 = ctrlDriver.update(self.omegaRefMot1, 
                                                         shares.sharedSpeedMot1, 
                                                         self.thetaRefEnc1, 
                                                         shares.sharedPositionEnc1)
                    self.newDutyMot2 = ctrlDriver.update(self.omegaRefMot2, 
                                                         shares.sharedSpeedMot2, 
                                                         self.thetaRefEnc2, 
                                                         shares.sharedPositionEnc2)
                    
                    print('CtrlTask: New Mot 1 L: ' + str(self.newDutyMot1) + ' New Mot 2 L: ' + str(self.newDutyMot2))
                    
                    # set motors to the new specified duty
                    mot1.setDuty(self.newDutyMot1)
                    mot2.setDuty(self.newDutyMot2)
                    

    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states.
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState Controls which state is the new state for the FSM.
        '''
        if self.debugFlag == True:
            print('CtrlTask: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            # print('State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state
    
    def readCSV(self, filename):
        '''
        @brief      Reads the CSV file to load in necessary reference data.
        @details    Since the CSV is overly sampled, the Front End will sample
                    the data at a specified rate, and then the controller task
                    will load the data as reference data for the controller
                    driver script.
        @param      filename Specifies which CSV file to read from.
        '''        
        with open(filename) as file:
            
            i = 0
            for currentData in file:
                # read the current line
                currentData = currentData.strip('\n')
                currentData = currentData.strip('\r')
                currentData = currentData.split(',')
                
                if self.debugFlag == True: 
                    print('CtrlTask: Currently on line: ' + str(i))
                
                # Assign the reference data to a specific array
                self.timeRef[i]  = float(currentData[0])
                self.omegaRef[i] = float(currentData[1])
                self.thetaRef[i] = float(currentData[2])
                
                if self.debugFlag == True: 
                    print('CtrlTask: timeRef is writing ' + str(self.timeRef[i]))
                    print('CtrlTask: omegaRef is writing ' + str(self.omegaRef[i]))
                    print('CtrlTask: thetaRef is writing ' + str(self.thetaRef[i]))
                
                # increase line counter
                i += 1
                
    def getCurrentRef(self, startTime, currentTime, currentI):
        '''
        @brief      Interpolation function to get new reference speed and position.
        @details    Since the CSV has less resolution in order to prevent memory 
                    errors, interpolation is needed in order to find the 
                    appropriate reference speed and theta to use in the 
                    controller driver. This function takes in the startTime 
                    and the currentTime in order to find the elapsed time since
                    starting the motors, as well as the current value for
                    interpolation iteration (in order to save time not looking
                    for a value from the start each time).
        @param      startTime Specifies the starting time of the motor.
        @param      currentTime Specifies the current time through the loop.
        @param      currentI Specifies the starting point for interpolation.
                    For the first round, this is 0, but once interpolation is 
                    performed, it will save the current iteration (equivalent
                    of saving the last time) so that it can be the basis for 
                    the next interpolation (essentially don't start from 
                    t = 0 again to save time of performance).
        '''    
        self.elapsedTime = utime.ticks_diff(currentTime, startTime)
        if self.debugFlag == True: 
            print('CtrlTask: Elapsed Time is: ' + str(self.elapsedTime))
            
        # Record the time for plotting later in the front end
        shares.sharedTimeSinceStart = self.elapsedTime/1000
        
        # Define the starting i (a counter) to use for the interpolation iteration
        i = currentI
        if self.debugFlag == True: 
            print('CtrlTask: Incoming i is: ' + str(i))
        
        # Compute the difference between the elapsed time (in ms) to the 
        # expected time for the current point in the reference file (marked by
        # the counter i)
        difference = self.elapsedTime/1000 - float(self.timeRef[i])
        if self.debugFlag == True: 
            print('CtrlTask: current difference is: ' + str(difference) + ' sec')

        # While the difference between the expected time and actual time is 
        # greater than the resolution of the sampled CSV file (25ms), it means
        # that the point doesn't lay between the points. 
        while difference > 0.025:
            # find the current difference
            difference = self.elapsedTime/1000 - float(self.timeRef[i])
            if self.debugFlag == True: 
                print('CtrlTask: current difference is: ' + str(difference) + ' sec')
            i += 1 # increment counter
        
        # Once the difference is bbelow 0.025s, it's known that we are between
        # two points in the CSV file. Then, we must interpolate between those 
        # two points in order to find an appropriate reference position and speed
        
        if self.debugFlag == True: 
            print('CtrlTask: Currently interpolationg between: ' + str(i) + ' and ' + str(i+1)) 
        
        # find the two adjacent times
        t1 = float(self.timeRef[i])
        t2 = float(self.timeRef[i+1])
        if self.debugFlag == True: 
            print('CtrlTask: Previous t: ' + str(t1))
            print('CtrlTask: Next t: ' + str(t2))
        
        # find the two adjacent positions
        theta1 = float(self.thetaRef[i])
        theta2 = float(self.thetaRef[i+1])
        if self.debugFlag == True: 
            print('CtrlTask: Previous theta: ' + str(theta1))
            print('CtrlTask: Next theta: ' + str(theta2))
        
        # find the two adjacent speeds
        omega1 = float(self.omegaRef[i])
        omega2 = float(self.omegaRef[i+1])
        if self.debugFlag == True: 
            print('CtrlTask: Previous omega: ' + str(omega1))
            print('CtrlTask: Next omega: ' + str(omega2))
        
        # Compute the new position and speed from interpolation
        newTheta = theta1 + (self.elapsedTime/1000 - t1)*((theta2 - theta1)/(t2 - t1))
        newOmega = omega1 + (self.elapsedTime/1000 - t1)*((omega2 - omega1)/(t2 - t1))
        if self.debugFlag == True: 
            print('CtrlTask: Setting new theta to: ' + str(newTheta) + ' deg')
            print('CtrlTask: Setting new speed to: ' + str(newOmega) + ' rpm')
        
        # Assign the current iteration element to what it left off at
        self.currentI = i
        if self.debugFlag == True: 
            print('CtrlTask: Setting I to: ' + str(self.currentI))
        
        return newTheta, newOmega