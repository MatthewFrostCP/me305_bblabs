'''
@file       shares.py
@brief      This file stores some shared variables between tasks
@details    Encoder positions, speeds, delta, times, and several other key
            parameters will be shared here. The ctrlTask generally will write
            to the file, and the UItask will grab the values and pass it 
            through serial for the Sypder Console to read.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/shares.py
@image      html Task_Diagram.jpg " " width = 40%
@details    \n <CENTER> **Figure 1.** Task Diagram for Motor Controller Final </CENTER> \n
@author     Matthew Frost
@date       Originally created on 02/25/21 \n Last modified on 03/17/21
'''
## @brief             This parameter tells the ctrl task to start
sharedStartCmd        = None

# These parameters generally control Encoder/Motor 1
## @brief             A character has been shared for Enc 1
sharedCmd1            = None 
## @brief             The position of Enc 1
sharedPositionEnc1    = 0    
## @brief             The delta of Enc 1
sharedDeltaEnc1       = 0    
## @brief             The speed of Enc 1
sharedSpeedMot1       = 0    

# These parameters generally control Encoder/Motor 2
## @brief             A character has been shared for Enc 2
sharedCmd2            = None 
## @brief             The position of Enc 2
sharedPositionEnc2    = 0    
## @brief             The delta of Enc 2
sharedDeltaEnc2       = 0     
## @brief             The speed of Enc 2
sharedSpeedMot2       = 0    
## @brief             

## @brief This command tells the ctrlTask to start using the motor control state
sharedStartCtrl       = False

## @brief             Keeps track of the time since the motor controller has started
sharedTimeSinceStart  = 0

## @brief             These variables hold the J parameters for encoder 1
sharedJ1              = 0
## @brief             These variables hold the J parameters for encoder 2
sharedJ2              = 0

## @brief             These variables hold the Kp parameter for the motor controller
sharedKpValue         = 0.05
## @brief             These variables hold the Ki parameter for the motor controller
sharedKiValue         = 0.05
