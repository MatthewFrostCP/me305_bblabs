# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 14:47:42 2021

@author: matth
"""

# if __name__ =='__main__':
#     # Adjust the following code to write a test program for your motor class. Any
#     # code within the if __name__ == '__main__' block will only run when the
#     # script is executed as a standalone program. If the script is imported as
#     # a module the code block will not run.
    
#     # Create the pin objects used for interfacing with the motor driver
#     pin_nSLEEP  = Pin.cpu.A15
#     pin_IN1     = Pin.cpu.B4
#     pin_IN2     = Pin.cpu.B5
#     pin_IN3     = Pin.cpu.B0
#     pin_IN4     = Pin.cpu.B1
    
#     # Create the timer object used for PWM generation
#     tim     = Timer(3, freq=20000);
    
#     ch1 = 1
#     ch2 = 2
#     ch3 = 3
#     ch4 = 4
    
#     # Create a motor object passing in the pins and timer
    
    
#     mot1     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, ch1, ch2)
#     mot2     = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, ch3, ch4)

#     # Enable the motor driver
#     mot2.enable()
    
#     # Set the duty cycle to 10 percent
#     mot2.set_duty(50)
#     delay(500)
#     print('Motor 1 should stop now')
#     mot2.disable()
    
#     delay(500)
    
#     mot1.enable()
    
#     mot1.set_duty(50)
#     delay(500)
    
#     mot1.disable()
