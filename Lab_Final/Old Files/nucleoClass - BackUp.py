# -*- coding: utf-8 -*-
'''
@file       nucleoClass.py
@brief      Class to run state machine on Nucleo
@details    Wait for user to input a `g`, then take data until it either times
            out after 30 seconds, or the user pressed `s.` Then, it will 
            compile the data and send it over to Spyder through serial to plot.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/nucleoClass.py
@author     Matthew Frost
@date       Originally created on 02/18/21 \n Last modified on 02/24/21
'''
import pyb
from pyb import UART
from array import array
from math import exp, sin, pi
import utime

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
# Assign variable to UART port 2
myuart = UART(2)

# while True:
class nucleoBackUp:
    S0_INIT             = 0   # basic init state
    S1_READY_FOR_INPUT  = 1   # Wait for user to press g
    S2_COLLECT_DATA     = 2   # sample data and store to array
    S3_PRINT_DATA       = 3   # print data back to PC
    S4_SEND_PLOT_CMD    = 4   # finished, send command for Spyder to plot
    S5_FINISHED         = 5   # program is finished
    
    
    def __init__(self, debugFlag = True):
        self.state          = 0 # start in the init state
        self.debugFlag      = debugFlag # used to view debugging print statements
        self.currentTime    = utime.ticks_ms()
        self.unitTime       = 50    # set the frequency for data to be sampled
        self.maxTime        = 30000 # max time is 30 seconds or 30000 ms
        self.index          = (self.maxTime/self.unitTime)+1
        self.times          = array('f', int(self.index)*[0]) # set up array for time values
        self.values         = array('f', int(self.index)*[0]) # set up array for function values
        self.currentElement = 0 # the current element within the array
        self.val            = 0 # set keyboard input variable to 0
        self.n              = 0 # set counter = 0
        
    def run(self): 
        self.currentTime = utime.ticks_ms()
        
        if self.state == self.S0_INIT:
            if self.debugFlag == True:
                print('Initializing program')
            self.transitionTo(self.S1_READY_FOR_INPUT)
            if self.debugFlag == True:
                print('if you would like to start taking data, please press g')
            
        elif self.state == self.S1_READY_FOR_INPUT:
            if myuart.any():
                self.val = myuart.readchar()
                print('You sent an ASCII '+ str(self.val) +' to the Nucleo')
                
            if self.val == 103:
                print('You have pressed g! starting data collection now')
                self.startTime = self.currentTime
                self.transitionTo(self.S2_COLLECT_DATA)
                    
        elif self.state == self.S2_COLLECT_DATA:    
            self.elapsedTime = utime.ticks_diff(self.currentTime,self.startTime)
            print('current elapsed time: ' + str(self.elapsedTime))
            
            if myuart.any():
                self.val = myuart.readchar()
                print('You sent an ASCII '+ str(self.val) +' to the Nucleo')
                
            if self.val == 115:
                print('You have pressed s, stopping data collection now')
                self.transitionTo(self.S3_PRINT_DATA)
            
            if self.currentElement <= self.maxTime/self.unitTime:  
                if self.elapsedTime > self.unitTime:
                    self.times[self.currentElement]  = self.currentElement*self.unitTime/1000
                    print('logged for times array: ' + str(self.times[self.currentElement]))
                    self.values[self.currentElement] = exp(-.1*self.currentElement*self.unitTime/1000)*sin(2*pi/3*self.currentElement*self.unitTime/1000)
                    print('logged for values array: ' + str(self.values[self.currentElement]))
                    self.currentElement += 1
                    print('current Element incremented to: ' + str(self.currentElement))
                    self.startTime = self.currentTime
                
            else:
                self.transitionTo(self.S3_PRINT_DATA)
                
        elif self.state == self.S3_PRINT_DATA:
            if self.debugFlag == True:
                print('printing data now')
                
            if self.n <= len(self.times)-1:
                # if self.n == 0:
                #     myuart.write('{:}, {:}\r\n'.format(self.times[self.n], self.values[self.n]))
                if self.n >= 1 and float(self.times[self.n]) != 0.0 and float(self.values[self.n]) != 0.0:
                    myuart.write('{:}, {:}\r\n'.format(self.times[self.n], self.values[self.n]))
                self.n +=1
            else:
                print('finished creating csv data')    
                self.transitionTo(self.S4_SEND_PLOT_CMD)
                
        elif self.state == self.S4_SEND_PLOT_CMD:
            myuart.write('generatePlot\r\n')
            self.transitionTo(self.S5_FINISHED)
            
        elif self.state == self.S5_FINISHED:
            pass
    
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                   directed to transition.
        '''
        if self.debugFlag == True:
            print('Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            # print('State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state
    
