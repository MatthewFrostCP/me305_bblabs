'''
@file       main.py
@brief      Default program on Nucleo to run FSM class
@details    Create an object of the nucleo class and set it to run 
            indefinitely
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/main.py
@author     Matthew Frost
@date       Originally created on 02/18/21 \n Last modified on 02/24/21
'''

from nucleoClass import nucleo

if __name__ == "__main__":
    # create an instance of the class 'nucleo' 
    task1 = nucleo(True)     
  
    while True: 
        try: 
            task1.run() 
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization
    print('Thanks for testing out the program. See you next time!')
