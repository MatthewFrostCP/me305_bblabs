# -*- coding: utf-8 -*-
import pyb

# def tick(timer):  
#     if timer == 'tim4':          
#         print('Encoder A is at: ' + str(timer.counter()))  
#     elif timer === 'tim8':
#         print('Encoder B is at: ' + str(timer.counter())) 
#     else:
#         print('idk what is happening')


tim4 = pyb.Timer(4, prescaler=0, period=65535)
tim8 = pyb.Timer(8, prescaler=0, period=65535)

t4ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B6)
t4ch2 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B7)
t8ch1 = tim8.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C6)
t8ch2 = tim8.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C7)


while True: 
    try: 
        # tim4.callback(tick)
        # tim8.callback(tick)
        print('Encoder 1 is at: ' + str(tim4.counter()))
        print('Encoder 2 is at: ' + str(tim8.counter()))
    except KeyboardInterrupt:
        break