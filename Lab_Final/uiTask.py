# -*- coding: utf-8 -*-
'''
@file       uiTask.py
@brief      Class to run state machine on Nucleo and communicate with Nucleo.
@details    Interface with the Spyder Console window through the serial 
            communication. If a keyboard key has been pressed, it will be sent
            through serial to this script, where it will call for an action, 
            like getting a motor position or speed. Data will be sent back 
            through serial to output into the Spyder Console or to plot.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab_Final/uiTask.py
@image      html UI_Task_FSM.jpg " " width = 30%
@details    \n <CENTER> **Figure 1.** Task Diagram for UI Task </CENTER> \n
@author     Matthew Frost
@date       Originally created on 02/18/21 \n Last modified on 03/17/21
'''
import pyb
from pyb import UART
from array import array
import utime
import shares
import gc

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

# while True:
class uiTask:
    ''' 
     @brief    UI Task interfaces between shares.py and the serial port.
     @details  This class will take readings from the shares.py file and 
               reports them through serial to the front end. It also receives
               characters/commands from the front end and sends them through
               shares.py for the ctrlTask file to read and execute.
    '''
    ## @brief           Basic init state
    S0_INIT             = 0   
    ## @brief           Wait for user to press a letter
    S1_READY_FOR_INPUT  = 1   
    ## @brief           Sample data and store to array
    S2_COLLECT_DATA     = 2 
    ## @brief           Print data back to PC
    S3_PRINT_DATA       = 3   
    ## @brief           Finished, send command for Spyder to plot
    S4_SEND_PLOT_CMD    = 4   
    ## @brief           Program is finished
    S5_FINISHED         = 5   
    
    def __init__(self, debugFlag = True, uiPeriod = 10):
        ''' 
        @brief    Initialize the UI task.
        @details  This task will interface with shares.py at regular intervals
                  and send information back through serial.
        @param    debugFlag Enables or prevents certain print statements from
                  appearing.
        @param    uiPeriod Specifies the rate at which the ui task is called, 
                  in milliseconds. This must be faster than the controller
                  task, or else the UI task will miss data points.
       '''
        ## @brief            Contains the information for which state its in   
        self.state           = 1 # start in the init state
        ## @brief            Debugging flag for detailed analysis while running
        self.debugFlag       = debugFlag # used to view debugging print statements
        ## @brief            Uses the utime module to keep track of time
        self.currentTime     = utime.ticks_ms()
        ## @brief            The period for the UI Task in ms
        self.uiPeriod        = uiPeriod
        ## @brief            Time stamp for the next iteration of the task
        self.nextTime        = utime.ticks_add(self.currentTime, self.uiPeriod)
        ## @brief            The max time before program times out in ms
        #  @details          This time is based on the given speed and position
        #                    curves for controller tracking.
        self.maxTime         = 15000 
        ## @brief            This helps create arrays for the speed and position
        self.index           = (self.maxTime/self.uiPeriod)+1
        ## @brief            Defines the Controller Task period in ms
        self.ctrlPeriod      = 25
        ## @brief            An array to store time data to plot
        self.times           = array('f', int(self.index)*[0]) # set up array for time values
        gc.collect()
        ## @brief            An array to store Enc 1 Angle data to plot
        self.valuesEnc1Angle = array('f', self.times) # set up array for function values
        gc.collect()
        ## @brief            An array to store Enc 1 Speed data to plot
        self.valuesEnc1Speed = array('f', self.times) # set up array for function values
        gc.collect()
        ## @brief            An array to store Enc 2 Angle data to plot
        self.valuesEnc2Angle = array('f', self.times) # set up array for function values
        gc.collect()
        ## @brief            An array to store Enc 2 Speed data to plot
        self.valuesEnc2Speed = array('f', self.times) # set up array for function values
        gc.collect()
        ## @brief            Counter to find the current element within arrays
        self.currentElement  = 0 # the current element within the array
        ## @brief            Holds the input commands (letters) from serial 
        self.val             = 0 # set keyboard input variable to 0
        ## @brief            Counter used for sending data one element at a time
        self.n               = 0 # set counter = 0
        ## @brief            Sends a signal to start data sending to console
        self.sendingDataFlag = 1 # counter for sending the data

        
    def run(self): 
        '''
        @brief      Report necessary measurements to the shares.py task.
        @details    This method updates shares.py with the keyboard inputs in
                    order for the controller task to collect the requested
                    values.
        '''
        self.currentTime = utime.ticks_ms() # find the current time
        
        # Establish a period at which the ui task is ran.
        if utime.ticks_diff(self.currentTime, self.nextTime) >= 0:
            if self.debugFlag == True:
                print('UI: Nucleo period reset')
            
            # time stamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.uiPeriod)

            if self.state == self.S0_INIT:
                print('UI: To zero the encoder, press z.'
                      ' To see the current position, press p.'
                      ' To see the current delta value, press d.'
                      ' To sample data for 30 seconds, press g.'
                      ' To stop data collection, press s.'
                      ' To start a motor, press m.'
                      ' To disable a motor, press b.'
                      ' To get the velocity, press v.'
                      ' For utilizing the second motor, capitalize the commands.')
                self.transitionTo(self.S1_READY_FOR_INPUT)
                
            elif self.state == self.S1_READY_FOR_INPUT:
                if myuart.any():
                    self.val = myuart.readchar()
                    print('UI: You sent an ASCII '+ str(self.val) +' to the Nucleo.')
                    shares.sharedStartCmd = 'Sending a character'
                
                # These key commands are for motor 1 (lower case)
                if self.val == 103:
                    print('UI: You have pressed g. Starting data collection now.')
                    shares.sharedCmd1 = 'g'
                    self.transitionTo(self.S2_COLLECT_DATA)
                    ## @brief      The time stamp for starting data collection 
                    self.startTime = self.currentTime
                    shares.sharedStartCtrl = True
                elif self.val == 112:
                    print('UI: You have pressed a p. Getting current Enc 1 positon now.')
                    shares.sharedCmd1 = 'p'
                    myuart.write('NUC: Last position at: ' + str(shares.sharedPositionEnc1))
                elif self.val == 100:
                    print('UI: You have pressed a d. Getting current Enc 1 delta now.')
                    shares.sharedCmd1 = 'd'
                    myuart.write('NUC: Last delta at: ' + str(shares.sharedDeltaEnc1))
                elif self.val == 122:
                    print('UI: You have pressed a z. Zeroing encoder Enc 1 position now.')
                    shares.sharedCmd1 = 'z'
                elif self.val == 109:
                    print('UI: You have pressed a m. Sending Mot 1 command now.')
                    shares.sharedCmd1 = 'm'
                elif self.val == 118:
                    print('UI: You have pressed a v. Getting speed for Mot 1.')
                    shares.sharedCmd1 = 'v'
                    myuart.write('NUC: Last velocity at: ' + str(shares.sharedSpeedMot1))
                elif self.val == 98:
                    print('UI: You have pressed a b. Stopping Motor 1.')
                    shares.sharedCmd1 = 'b'
                
                # These key commands are for motor 2 (upper case)
                elif self.val == 80:
                    print('UI: You have pressed a P. Getting current Enc 2 positon now.')
                    shares.sharedCmd2 = 'P'
                    myuart.write('NUC: Last position at: ' + str(shares.sharedPositionEnc2))
                elif self.val == 68:
                    print('UI: You have pressed a D. Getting current Enc 2 delta now.')
                    shares.sharedCmd2 = 'D'
                    myuart.write('NUC: Last delta at: ' + str(shares.sharedDeltaEnc2))
                elif self.val == 90:
                    print('UI: You have pressed a Z. Zeroing encoder Enc 2 position now.')
                    shares.sharedCmd2 = 'Z'
                elif self.val == 77:
                    print('UI: You have pressed a M. Sending Mot 2 command now.')
                    shares.sharedCmd2 = 'M'
                elif self.val == 86:
                    print('UI: You have pressed a V. Getting speed for Mot 2.')
                    shares.sharedCmd2 = 'V'
                    myuart.write('NUC: Last velocity at: ' + str(shares.sharedSpeedMot2))
                elif self.val == 66:
                    print('UI: You have pressed a B. Stopping Motor 2.')
                    shares.sharedCmd2 = 'B'
                else:
                    pass
                self.val = None     # Reset self.val 
                        
            elif self.state == self.S2_COLLECT_DATA: 
                ## @brief The elapsed time since starting data collection
                self.elapsedTime = utime.ticks_diff(self.currentTime, self.startTime)
                if self.debugFlag == True:
                    print('UI: current elapsed time: ' + str(self.elapsedTime))
                
                if myuart.any():
                    self.val = myuart.readchar()
                    print('UI: You sent an ASCII '+ str(self.val) +' to the Nucleo')
                
                # Potential commands that can stop the data collection or motors
                # when motor is collecting data and running.
                if self.val == 115:
                    print('UI: You have pressed s, stopping data collection now')
                    shares.sharedCmd1 = 's'
                    self.transitionTo(self.S3_PRINT_DATA)
                elif self.val == 98:
                    print('UI: You have pressed a b. Stopping Motor.')
                    shares.sharedCmd1 = 'b'
                elif self.val == 66:
                    print('UI: You have pressed a B. Stopping Motor 2.')
                    shares.sharedCmd2 = 'B'   

                if self.currentElement <= self.maxTime/self.ctrlPeriod:  
                    if self.elapsedTime > self.ctrlPeriod:
                        self.times[self.currentElement] = shares.sharedTimeSinceStart
                        self.valuesEnc1Angle[self.currentElement] = shares.sharedPositionEnc1
                        self.valuesEnc1Speed[self.currentElement] = shares.sharedSpeedMot1
                        self.valuesEnc2Angle[self.currentElement] = shares.sharedPositionEnc2
                        self.valuesEnc2Speed[self.currentElement] = shares.sharedSpeedMot2
                        if self.debugFlag == True:
                            print('UI: Saving: Time: ' + str(self.times[self.currentElement]) + 
                                  ', Enc 1 Angle: ' + str(self.valuesEnc1Angle[self.currentElement]) + 
                                  ', Enc 1 Speed: ' + str(self.valuesEnc1Speed[self.currentElement]) + 
                                  ', Enc 2 Angle: ' + str(self.valuesEnc2Angle[self.currentElement]) + 
                                  ', Enc 2 Speed: ' + str(self.valuesEnc2Speed[self.currentElement])) 
                        self.currentElement += 1
                        self.startTime = self.currentTime # Reset startTime
  
                else:
                    self.transitionTo(self.S3_PRINT_DATA)
                    
            elif self.state == self.S3_PRINT_DATA:
                if self.debugFlag == True:
                    print('UI: sendingDataFlag: ' + str(self.sendingDataFlag))
                
                if self.sendingDataFlag == 1:
                    if self.debugFlag == True:
                        print('UI: printing data now.')
                        print('UI: self.n is: ' + str(self.n))
                    if self.n <= self.currentElement-1:
                        if self.debugFlag == True:
                            print('UI: Writing: {:}, {:}, {:}, {:}, {:} \r\n'.format(self.times[self.n], 
                                                                self.valuesEnc1Angle[self.n],
                                                                self.valuesEnc1Speed[self.n],
                                                                self.valuesEnc2Angle[self.n],
                                                                self.valuesEnc2Speed[self.n]))
                        myuart.write('{:}, {:}, {:}, {:}, {:} \r\n'.format(self.times[self.n], 
                                                            self.valuesEnc1Angle[self.n],
                                                            self.valuesEnc1Speed[self.n],
                                                            self.valuesEnc2Angle[self.n],
                                                            self.valuesEnc2Speed[self.n]))
                        self.n +=1
                    else: 
                        self.transitionTo(self.S4_SEND_PLOT_CMD) 

                else:
                    if self.debugFlag == True:
                        print('UI: finished creating csv data')    
                    self.transitionTo(self.S4_SEND_PLOT_CMD)
                    
            elif self.state == self.S4_SEND_PLOT_CMD:
                myuart.write('J{:},{:},{:},{:}\r\n'.format(shares.sharedKpValue,
                                                           shares.sharedKiValue,
                                                           shares.sharedJ1,
                                                           shares.sharedJ2))
                myuart.write('generatePlot\r\n')
                self.transitionTo(self.S5_FINISHED)
                
            elif self.state == self.S5_FINISHED:
                pass
    
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState Controls which state is the new state for the FSM.
        '''
        if self.debugFlag == True:
            print('UI: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
        self.state = newState # now that transition has been declared, update state