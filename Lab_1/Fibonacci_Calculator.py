'''@file        Fibonacci_Calculator.py
   @brief       Calculates the Fibonacci number at a specific index
   @details     Queries the user to input an index of the Fibonacci sequence 
                and calculates the resulting value using a bottom-up approach
                to allow for fast computations
                Link: https://bitbucket.org/MatthewFrostCP/me305_bblabs/src/master/Lab%201/Fibonacci_Calculator.py
   @author      Matthew Frost
   @date       1/20/2021
   @copyright   License Info Here
'''

def fib (idx):
    '''
        @brief      Calculates the Fibonacci number at a specific index
        @details    After the user inputs an index, the function uses a 
                    bottom-up approach to solving for the Fibonacci number
                    by storing the last two calculations (thus reusing them to
                    save compution time).
        @param      idx The index of the Fibonacci sequence the user intends to
                        find the value of
        @return     The Fibonacci number at the specified index

    '''   
    # Define two variables that will be placeholders for th ebottom-up approach
    TwoBehind = 0
    OneBehind = 1
    i = 1
    
    if idx < 0: # This is to make sure the number is positive
        print('Please input a valid number.')
        
    elif idx == 0: # if idx = 0, display 0 (first number in sequence)
        return 0
    
    elif idx == 1: # if idx = 1, display 1 (second number in sequence)
        return 1
    
    # other than idx = 0 or 1, calculate the Fibonacci number
    else:
        while i < idx:
            fib_num = OneBehind + TwoBehind # set the output (fib_num) equal
                                              # to the sum of the value at index
                                              # two behind + index one behind
            TwoBehind = OneBehind           # Shift the values so that the 
                                              # value 2 behind is now the value
                                              # that was previously one behind
            OneBehind = fib_num             # Do the same with the value that 
                                              # was one behind, but make it 
                                              # equal to the sum calculated for
                                              # the fib_num
            i += 1                          # increment the index by one
        
        return fib_num                      # return the last saved value 
    
if __name__ == '__main__':
    # Welcome the user to the Fibonacci Calculator
    print('Thanks for initializing the Fibonacci Calculator!')

    while True:
        try:
            # Query the user for an index of which to calculate
            user_input = input('Please enter the index of the Finbonacci sequence '
                               'you wish to calculate: ')
            
            # Check to see if the user inputted a valid integer
            if user_input.isdigit() == False:
                print('\r\nPlease input a valid integer.')
            
            # if valid, run the index through the function to calculate the 
            # Fibonacci number.
            else:
                idx = int(user_input)
                print('\r\nAt an index of {:}, the corresponding '
                      'Fibonacci number is {:}.'.format(idx,fib(idx)))

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Thank the user for coming    
    print('Thanks for trying the Fibonacci Calculator!')